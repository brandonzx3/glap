import { global } from "./index";
import { ToServerMsg } from "./codec";
import { emojify, unemojify, emojie_selector_open, toggleEmoteArea } from "./emojies";

let message_button = (document.querySelector("#chat_send") as HTMLInputElement);
let message_box = (document.querySelector("#chat_input") as HTMLInputElement);
let message_root = (document.querySelector("#chat_messages") as HTMLDivElement);
const root_root = document.querySelector("#chat_root_root") as HTMLDivElement;
const chat_toggle = document.querySelector("#chat_toggle") as HTMLButtonElement;
const chat_root = document.querySelector("#chat_root") as HTMLDivElement;

message_box.addEventListener("keydown", key => {
    if(key.keyCode == 13) {
		global.chat.send_message(message_box.value);
        message_box.value = "";
		key.stopPropagation();
    }

    if(emojie_selector_open) { toggleEmoteArea(); }

    setTimeout(function() {
        if(key.shiftKey && key.keyCode == 59) {
            message_box.value = emojify(message_box.value);
        }        
    }, 10);
});

message_button.addEventListener("keydown", e => { e.stopPropagation(); });
message_button.addEventListener("keypress", e => { e.stopPropagation(); });
message_button.addEventListener("keyup", e => { e.stopPropagation(); });

message_button.onclick = function() { 
    global.chat.send_message(message_box.value);
    if(emojie_selector_open) { toggleEmoteArea(); }
}

export class Chat {
	constructor() {
	   chat_toggle.addEventListener("click", _ => { this.toggle(); });
	}

    is_open = false;
	notifications: HTMLDivElement[] = [];

	toggle() {
		if (!this.is_open) this.open();
		else this.close();
	}

    open() {
        if(this.is_open) return;
        this.is_open = true;
		chat_toggle.innerText = "Close";
		chat_root.classList.add("open");
		for (const message of this.notifications) message.classList.add("outro");
		this.notifications = [];
		message_box.blur();
        if(emojie_selector_open) { toggleEmoteArea(); }
    }

    close() {
        if(!this.is_open) return;
        this.is_open = false;
		chat_toggle.innerText = "Chat";
		chat_root.classList.remove("open");
        if(emojie_selector_open) { toggleEmoteArea(); }
    }

    receive_message(content: string, username: string, color: string) {
        console.log(content);
        let temp = (document.querySelector("#message_template") as HTMLTemplateElement);
        let clone = (temp.content.cloneNode(true) as HTMLDivElement);
        let message = (clone.firstElementChild as HTMLParagraphElement);
        content = emojify(content)
        message.innerText = `${username}: ${content}`;
        message.style.color = color;
        message_root.appendChild(clone);
        message_root.scrollTop = message_root.offsetTop + message_root.scrollHeight;
        if(!this.is_open) {
            let notification = document.createElement("div") as HTMLDivElement;
			notification.classList.add("chat_notification");
            notification.innerText = `${username}: ${content}`;
            notification.style.color = color;
            root_root.insertBefore(notification, chat_root);
			this.notifications.push(notification);
            if(this.notifications.length > 3) this.notifications.shift().classList.add("outro");
            setTimeout(() => {
				const i = this.notifications.indexOf(notification);
				if (i > -1) this.notifications.splice(i, 1);
				notification.classList.add("outro");
				setTimeout(() => { notification.parentElement.removeChild(notification); }, 1000);
            }, 9500);
        }
    }

    send_message(content: string) {
        console.log(content);
        content = unemojify(content);
        if(content.replace(/\s/g, '').length) {
            if(content.length >= 201) {
                content = content.substring(0, 201);
            }
            global.socket.send(new ToServerMsg.SendChatMessage(content).serialize());
        }
		message_box.blur();
        message_box.value = "";
    }
}

