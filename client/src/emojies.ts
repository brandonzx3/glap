import { emojies } from "../dist/emoji_compact";

let message_box = (document.querySelector("#chat_input") as HTMLInputElement);
const chat_emoji_button = document.querySelector("#chat_emojies") as HTMLDivElement;
const emote_select_div = document.querySelector("#emote_select") as HTMLDivElement;

export var emojie_selector_open = false;


Object.entries(emojies).forEach(([key, value], index) => {
    let p = document.createElement("p");
    p.innerText = value.toString();
    emote_select_div.appendChild(p);
    p.addEventListener('click', function() {
        message_box.value = message_box.value + p.innerText;
        message_box.focus();
    })
});


export function toggleEmoteArea() {
    emojie_selector_open = !emojie_selector_open;
    if(!emojie_selector_open) {
        emote_select_div.style.opacity = "0%";
        setTimeout(() => {
            emote_select_div.style.visibility = "hidden";
        }, 100);
        chat_emoji_button.style.color = "white";
    } else {
        emote_select_div.style.visibility = "visible";
        emote_select_div.style.opacity = "100%";
        chat_emoji_button.style.color = "#ffcc4d";
    }
}

chat_emoji_button.addEventListener('click', toggleEmoteArea);

export function emojify(message: string) {
    var emojied: string = message;
    for(var sus = 0; sus < emojied.length; sus++) {
        Object.entries(emojies).forEach(([key, value], index) => {
            emojied = emojied.replace(`:${key}:`, value.toString());
        });
    }
    return emojied;
}

//will be replaced by encoding and decoding in netcode
export function unemojify(message: string) {
    var unemojied: string = message;
    for(var sus = 0; sus < unemojied.length; sus++) {
        Object.entries(emojies).forEach(([key, value], index) => {
            unemojied = unemojied.replace(value.toString(), `:${key}:`);
        });
    }
    return unemojied;
}