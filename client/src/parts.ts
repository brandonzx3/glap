import { PartKind } from "./codec";
import { PlayerMeta, global } from "./index";
import * as PIXI from 'pixi.js';
import * as Particles from '@pixi/particle-emitter';
import { ParticleManager, ThrusterEmitter, ThrusterParticleConfig, SuperThrusterParticleConfig, EcoThrusterParticleConfig, CoreParticleConfig } from "./particles";
import { Box } from "./codec";
import { Interpolation, HasInterpolation } from "./interpolation";

const HALF_PI = Math.PI / 2;

export class PartMeta implements HasInterpolation {
    id: number;
    sprite: PIXI.Sprite;
    connector_sprite: PIXI.Sprite = null;
    kind: PartKind;
	hidden = false;
	thrust_particles: ParticleManager;
    constructor(id: number, kind: PartKind) {
        this.id = id;
        this.kind = kind;
        this.sprite = new PIXI.Sprite();
        this.sprite.width = 1; this.sprite.height = 1;
        this.update_sprites();
        if (kind === PartKind.Core) {
			this.sprite.anchor.set(0.5,0.5);
			this.thrust_particles = new CoreParticleManager(this);
		} else if (kind === PartKind.LandingThrusterSuspension) {
			this.sprite.anchor.set(0.5,0);
			this.sprite.height = 0.4;
		} else if (kind === PartKind.LandingWheelSuspension) {
			this.sprite.anchor.set(0.5, 0.5);
		} else {
			this.sprite.anchor.set(0.5,1);
			switch (kind) {
				case PartKind.LandingThruster:
				case PartKind.Thruster:
				case PartKind.EcoThruster:
				case PartKind.SuperThruster:
				case PartKind.HubThruster:
					this.thrust_particles = new ThrustParticleManager(this); break;
			}
		}
        global.part_sprites.addChild(this.sprite);

        this.connector_sprite = new PIXI.Sprite(global.spritesheet.textures["connector.png"]);
        this.connector_sprite.width = 0.333; this.connector_sprite.height = 0.15;
        this.connector_sprite.anchor.set(0.5,0);
    }
    owning_player: PlayerMeta = null;
    thrust_mode = new CompactThrustMode(0);

    x = 0;
    y = 0;
    rot = 0;
	particle_speed_x = 0;
	particle_speed_y = 0;

	inter_x = new Interpolation();
	inter_y = new Interpolation();
	inter_rot = new Interpolation();
	interpolations = [ this.inter_x, this.inter_y, this.inter_rot ];
	inter_set_next_dest() {
		this.inter_x.dest = this.x;
		this.inter_y.dest = this.y;
		this.inter_rot.dest = this.rot;
		let diff = this.inter_rot.dest - this.inter_rot.now;
		while (Math.abs(diff) > Math.PI) {
			if (diff > Math.PI) this.inter_rot.now += Math.PI * 2;
			else if (diff < -Math.PI) this.inter_rot.now -= Math.PI * 2;
			diff = this.inter_rot.dest - this.inter_rot.now;
		}
	}
	after_update(_delta_ms: number) {
		this.sprite.x = this.inter_x.now;
		this.sprite.y = this.inter_y.now;
		this.sprite.rotation = this.inter_rot.now;
		this.connector_sprite.position.copyFrom(this.sprite.position);
		this.connector_sprite.rotation = this.sprite.rotation;
		if (this.kind === PartKind.Core && this.owning_player != null) {
			this.owning_player.name_sprite.position.set(this.inter_x.now, this.inter_y.now - 0.85);
		}
	}

    update_sprites() {
        switch (this.kind) {
            case PartKind.Core: this.sprite.texture = global.spritesheet.textures["core.png"]; break;
            case PartKind.Cargo: this.sprite.texture = global.spritesheet.textures[this.owning_player !== null ? "cargo.png" : "cargo_off.png"]; break;
            case PartKind.LandingThruster: this.sprite.texture = global.spritesheet.textures[this.owning_player !== null ? "landing_thruster.png" : "landing_thruster_off.png"]; break;
            case PartKind.Hub: this.sprite.texture = global.spritesheet.textures[this.owning_player !== null ? "hub.png" : "hub_off.png"]; break;
            case PartKind.SolarPanel: this.sprite.texture = global.spritesheet.textures[this.owning_player !== null ? "solar_panel.png" : "solar_panel_off.png"]; break;
			case PartKind.Thruster: this.sprite.texture = global.spritesheet.textures[this.owning_player !== null ? "thruster.png" : "thruster_off.png"]; break;
			case PartKind.EcoThruster: this.sprite.texture = global.spritesheet.textures[this.owning_player !== null ? "eco_thruster.png" : "eco_thruster_off.png"]; break;
			case PartKind.SuperThruster: this.sprite.texture = global.spritesheet.textures[this.owning_player !== null ? "super_thruster.png" : "super_thruster_off.png"]; break;
			case PartKind.PowerHub: this.sprite.texture = global.spritesheet.textures[this.owning_player !== null ? "power_hub.png" : "power_hub_off.png"]; break;
			case PartKind.HubThruster: this.sprite.texture = global.spritesheet.textures[this.owning_player !== null ? "hub_thruster.png" : "hub_thruster_off.png"]; break;
			case PartKind.LandingThrusterSuspension: this.sprite.texture = global.spritesheet.textures["landing_thruster_suspension.png"]; break; 
			case PartKind.LandingWheel: this.sprite.texture = global.spritesheet.textures[this.owning_player !== null ? "landing_wheel.png" : "landing_wheel.png"]; break;
			case PartKind.LandingWheelSuspension: this.sprite.texture = global.spritesheet.textures["landing_wheel_wheel.png"]; break;
			
            default: this.sprite.texture = global.spritesheet.textures["core.png"]; break;
        }
        global.connector_sprites.removeChild(this.connector_sprite);
        if (
			this.owning_player !== null && this.kind !== PartKind.Core
			&& this.kind !== PartKind.LandingThrusterSuspension
			&& this.kind !== PartKind.LandingWheelSuspension
		) global.connector_sprites.addChild(this.connector_sprite);
    }

	hide() {
		this.hidden = true;
		this.sprite.visible = false;
		this.connector_sprite.visible = false;
	}
	unhide() {
		this.hidden = false;
		this.sprite.visible = true;
		this.connector_sprite.visible = true;
	}

    update_thruster_sprites(thrust_forward: boolean, thrust_backward: boolean, thrust_clockwise: boolean, thrust_counter_clockwise: boolean) {     
		let am_i_thrusting;
		switch (this.thrust_mode.horizontal) {
			case HorizontalThrustMode.Clockwise: am_i_thrusting = thrust_clockwise; break;
			case HorizontalThrustMode.CounterClockwise: am_i_thrusting = thrust_counter_clockwise; break;
			case HorizontalThrustMode.Either: am_i_thrusting = false; break;
		}
		switch (this.thrust_mode.vertical) {
			case VerticalThrustMode.Forwards: am_i_thrusting = am_i_thrusting || thrust_forward; break;
			case VerticalThrustMode.Backwards: am_i_thrusting = am_i_thrusting || thrust_backward; break;
			//case VerticalThrustMode.None: break;
		}
		/*switch (this.kind) {
			case PartKind.Core: {
				this.thrust_sprites.children[0].visible = thrust_forward || thrust_clockwise
				this.thrust_sprites.children[1].visible = thrust_forward || thrust_counter_clockwise;
				this.thrust_sprites.children[2].visible = thrust_backward || thrust_counter_clockwise;
				this.thrust_sprites.children[3].visible = thrust_backward || thrust_clockwise;
			}; break;

			case PartKind.LandingThruster: this.thrust_sprites.children[0].visible = am_i_thrusting; break;
		}*/

	    am_i_thrusting = am_i_thrusting && !this.hidden;

		switch (this.kind) {
			case PartKind.LandingThruster:
			case PartKind.Thruster:
			case PartKind.EcoThruster:
			case PartKind.HubThruster:
			case PartKind.SuperThruster:
				(this.thrust_particles as ThrustParticleManager).emitter.emit = am_i_thrusting; 
				global.emitters.add(this.thrust_particles);
				break;
			case PartKind.Core:
				const particles = this.thrust_particles as CoreParticleManager;
				particles.bottom_left.emit = thrust_forward || thrust_clockwise;
				particles.bottom_right.emit = thrust_forward || thrust_counter_clockwise;
				particles.top_left.emit = thrust_backward || thrust_counter_clockwise;
				particles.top_right.emit = thrust_backward || thrust_clockwise;
				global.emitters.add(this.thrust_particles);
				break;
		}
    }
}

class ThrustParticleManager implements ParticleManager {
	parent: PartMeta;
	emitter: ThrusterEmitter;

	constructor(parent: PartMeta) {
		this.parent = parent;
		let offset;
		let vel;
		switch (parent.kind) {
			case PartKind.LandingThruster: 
			case PartKind.Thruster: 
			case PartKind.EcoThruster:
			case PartKind.HubThruster:
			case PartKind.SuperThruster: {
				offset = new PIXI.Point(0, -1);
				vel = new PIXI.Point(0, -3);
			}; break;

			case PartKind.Core: throw new Error("Didn't use CoreParticleManager");
		};

		let config;
		switch (parent.kind) {
			case PartKind.SuperThruster: config = SuperThrusterParticleConfig; break;
			case PartKind.EcoThruster: config = EcoThrusterParticleConfig; break;
			default: config = ThrusterParticleConfig;
		}
		this.emitter = new ThrusterEmitter(this.parent, offset, vel, global.thrust_particles, config);
	}

	update_particles(delta_seconds: number): boolean {
		this.emitter.updateOwnerPos(this.parent.sprite.x, this.parent.sprite.y);
		this.emitter.update(delta_seconds);
		return !this.emitter.emit && this.emitter.particleCount < 1;
	}
}


export class CoreParticleManager implements ParticleManager {
	parent: PartMeta;
	bottom_left: ThrusterEmitter;
	bottom_right: ThrusterEmitter;
	top_left: ThrusterEmitter;
	top_right: ThrusterEmitter;

	constructor(parent: PartMeta) {
		this.parent = parent;
		const magnitude = 1.5;
		this.bottom_left = new ThrusterEmitter(this.parent, new PIXI.Point(-0.4, 0.5), new PIXI.Point(0, magnitude), global.thrust_particles, CoreParticleConfig);
		this.bottom_right = new ThrusterEmitter(this.parent, new PIXI.Point(0.4, 0.5), new PIXI.Point(0, magnitude), global.thrust_particles, CoreParticleConfig);
		this.top_left = new ThrusterEmitter(this.parent, new PIXI.Point(-0.4, -0.5), new PIXI.Point(0, -magnitude), global.thrust_particles, CoreParticleConfig);
		this.top_right = new ThrusterEmitter(this.parent, new PIXI.Point(0.4, -0.5), new PIXI.Point(0, -magnitude), global.thrust_particles, CoreParticleConfig);
	}

	update_particles(delta_seconds: number): boolean {
		const self = this;
		function update_emitter(emitter: Particles.Emitter) {
			if (emitter.emit) {
				emitter.updateOwnerPos(self.parent.sprite.x, self.parent.sprite.y);
			}
			emitter.update(delta_seconds);
		}

		update_emitter(this.bottom_left);
		update_emitter(this.bottom_right);
		update_emitter(this.top_left);
		update_emitter(this.top_right);
		return !this.bottom_left.emit && this.bottom_left.particleCount < 1
			&& !this.bottom_right.emit && this.bottom_right.particleCount < 1
			&& !this.top_left.emit && this.top_left.particleCount < 1
			&& !this.top_right.emit && this.top_right.particleCount < 1;
	}
}

export enum HorizontalThrustMode { Clockwise, CounterClockwise, Either }
export enum VerticalThrustMode { Forwards, Backwards, None }

export class CompactThrustMode {
    dat: number;
    constructor(dat: number) { this.dat = dat; }
    get horizontal(): HorizontalThrustMode {
        switch (this.dat & 0b00000011) {
            case 0b00000001: return HorizontalThrustMode.Clockwise;
            case 0b00000000: return HorizontalThrustMode.CounterClockwise;
            case 0b00000010: return HorizontalThrustMode.Either;
        }
    }
    set horizontal(horizontal: HorizontalThrustMode) {
        let representation;
        switch (horizontal) {
            case HorizontalThrustMode.Clockwise: representation = 0b00000001; break;
            case HorizontalThrustMode.CounterClockwise: representation = 0b00000000; break;
            case HorizontalThrustMode.Either: representation = 0b00000010; break;
        };
        this.dat = (this.dat & 0b11111100) | representation;
    }
    get vertical(): VerticalThrustMode {
        switch (this.dat & 0b00001100) {
            case 0b00000100: return VerticalThrustMode.Forwards;
            case 0b00000000: return VerticalThrustMode.Backwards;
            case 0b00001000: return VerticalThrustMode.None;
            default: throw new Error();
        }
    }
    set vertical(vertical: VerticalThrustMode) {
        let representation;
        switch (vertical) {
            case VerticalThrustMode.Forwards: representation = 0b00000100; break;
            case VerticalThrustMode.Backwards: representation = 0b00000000; break;
            case VerticalThrustMode.None: representation = 0b00001000; break;
        }
        this.dat = (this.dat & 0b11110011) | representation;
    }

    static compose(horizontal: HorizontalThrustMode, vertical: VerticalThrustMode): CompactThrustMode {
        let thrust = new CompactThrustMode(0);
        thrust.horizontal = horizontal;
        thrust.vertical = vertical;
        return thrust;
    }
}
