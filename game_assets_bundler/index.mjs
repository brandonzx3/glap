import { Atlasify, Options } from "atlasify";
import lib_fs from "fs";
import lib_path from "path";

const __dirname = (() => {
    let maybe_dirname = new URL(import.meta["url"]).pathname;
    if (process.platform.indexOf("win") > -1) maybe_dirname = maybe_dirname.slice(1);
    return lib_path.dirname(maybe_dirname);
})();

(async () => {
	const options = new Options("spritesheet_rs.png", 20000000, 20000000, 2, "JsonHash");
	options.trimAlpha = false;

	const dirs = [lib_path.resolve(__dirname, "../game_assets")];
	let images = [];
	while (dirs.length > 0) {
		const dir = dirs.shift();
		const items = await lib_fs.promises.readdir(lib_path.resolve(dir), { withFileTypes: true });
		for (const item of items) {
			const fullpath = lib_path.join(dir, item.name);
			if (item.isFile() && item.name.length > 4 && (item.name.substring(item.name.length-4) === ".png" || item.name.substring(item.name.length-4) === ".jpg")) images.push(fullpath);
			else if (item.isDirectory()) dirs.push(fullpath);
		}
	}
	images = images.filter(path => path.indexOf("ship_editor") < 0);

	const packer = new Atlasify(options);
	await packer.addURLs(images);
	await packer.pack();
	if (packer.spritesheets.length !== 1) throw new Error("Died of spritesheets too many");

	await lib_fs.promises.writeFile(lib_path.resolve("../client/dist/spritesheet_rs.json"), packer.exporter.compile(packer.spritesheets[0]));
	await packer.atlas[0].image.write("../client/dist/spritesheet_rs.png");

})();


(async () => {
	const options = new Options("spritesheet_ship_editor.png", 4000, 4000, 2, "JsonHash");
	options.trimAlpha = false;

	const base = lib_path.resolve(__dirname, "../game_assets");
	const images = [
		base + "/cargo.png",
		base + "/core.png",
		base + "/eco_thruster.png",
		base + "/hub.png",
		base + "/hub_thruster.png",
		base + "/landing_thruster_ship_editor.png",
		base + "/landing_wheel_ship_editor.png",
		base + "/power_hub.png",
		base + "/solar_panel.png",
		base + "/super_thruster.png",
		base + "/thruster.png",
	];

	const packer = new Atlasify(options);
	await packer.addURLs(images);
	await packer.pack();
	if (packer.spritesheets.length !== 1) throw new Error("Died of spritesheets too many");

	await lib_fs.promises.writeFile(lib_path.resolve("../client/dist/spritesheet_ship_editor.json"), packer.exporter.compile(packer.spritesheets[0]));
	await packer.atlas[0].image.write("../client/dist/spritesheet_ship_editor.png");
})();
