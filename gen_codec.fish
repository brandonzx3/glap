#!/usr/bin/fish
cd (dirname (status filename))

pushd codec
cargo run
popd

cp codec/codec.rs server/src/codec.rs
cp codec/codec.ts server/src/codec.ts
