use super::{MyUnits, PartHandle};
use std::collections::BTreeMap;
use rapier2d::dynamics::{RigidBody, RigidBodyBuilder, BodyStatus, RigidBodyHandle, RigidBodySet, CoefficientCombineRule};
use rapier2d::geometry::{ColliderBuilder, SharedShape, Collider, ColliderSet, ColliderHandle};
use rapier2d::pipeline::ActiveEvents;
use super::typedef::*;
use crate::codec::{ PlanetKind, ToClientMsg, BeamoutKind };
use crate::storage7573::Storage7573;
use rand::Rng;
use super::parts::PartKind;

use Storage7573::Planet;

pub const SUN_ID: u8        = 00;
pub const EARTH_ID: u8      = 01;
pub const MOON_ID: u8       = 02;
pub const MERCURY_ID: u8    = 03;
pub const VENUS_ID: u8      = 04;
pub const MARS_ID: u8       = 05;
pub const JUPITER_ID: u8    = 06;
pub const SATURN_ID: u8     = 07;
pub const URANUS_ID: u8     = 08;
pub const NEPTUNE_ID: u8    = 09;
pub const PLUTO_ID: u8      = 10;
pub const TRADE_ID: u8      = 11;

pub const EARTH_ORBIT_DURATION: u32     = 3600  * 20 * 3;
pub const MOON_ORBIT_DURATION: u32      = 600   * 20 * 3;
pub const MARS_ORBIT_DURATION: u32      = 4800  * 20 * 3;
pub const MERCURY_ORBIT_DURATION: u32   = 1200  * 20 * 3;
pub const VENUS_ORBIT_DURATION: u32     = 2400  * 20 * 3;
pub const JUPITER_ORBIT_DURATION: u32   = 8400  * 20 * 6;
pub const SATURN_ORBIT_DURATION: u32    = 9600  * 20 * 7;
pub const URANUS_ORBIT_DURATION: u32    = 11520 * 20 * 8;
pub const NEPTUNE_ORBIT_DURATION: u32   = 13200 * 20 * 9;
pub const PLUTO_ORBIT_DURATION: u32     = 3394  * 20 * 3;

pub struct Planets {
    pub planets: BTreeMap<u8, CelestialObject>,
    pub earth_id: u8,
    pub trade_id: u8,
    pub sun_id: u8,
    pub planet_ids: Vec<u8>,
}
impl Planets {
    fn make_planet(id: u8, kind: PlanetKind, mass: f32, radius: f32, position: (f32, f32), cargo_upgrade: Option<PartKind>, orbit: Option<Orbit>, beamout_kind: BeamoutKind, bodies: &mut RigidBodySet, colliders: &mut ColliderSet, planets: &mut BTreeMap<u8, CelestialObject>) {
        let body = RigidBodyBuilder::new_dynamic()
            .additional_mass(0.001)
            .user_data(Planet(id).into())
            .translation(Vector::new(position.0, position.1))
            .dominance_group(127)
            .build();
        let body_handle = bodies.insert(body);
        let collider = ColliderBuilder::new(SharedShape::ball(radius))
            .user_data(Storage7573::Planet(id).into())
            //.active_events(ActiveEvents::CONTACT_EVENTS)
            .build();
        colliders.insert_with_parent(collider, body_handle, bodies);
        let cargo_sensor = ColliderBuilder::new(SharedShape::ball(radius + 0.3))
            .user_data(Storage7573::Planet(id).into())
            .active_events(ActiveEvents::INTERSECTION_EVENTS)
            .sensor(true)
            .build();
        colliders.insert_with_parent(cargo_sensor, body_handle, bodies);
        
        planets.insert(id, CelestialObject {
            kind,
            mass,
            orbit,
            beamout: beamout_kind,
            body_handle,
            cargo_upgrade,
            radius,
            position,
        });
    }

    pub fn new(bodies: &mut RigidBodySet, colliders: &mut ColliderSet) -> Planets {
        const EARTH_MASS: f32 = 600.0;
        const EARTH_SIZE: f32 = 25.0;
        let mut rand = rand::thread_rng();

        let mut planets = BTreeMap::new();

        let sun = Planets::make_planet(
            SUN_ID, PlanetKind::Sun,
            EARTH_MASS * 50.0, EARTH_SIZE * 4.7,
            (0.0, 0.0),
            None, None, BeamoutKind::None,
            bodies, colliders, &mut planets,
        );

        let earth_ticks_ellapsed = rand.gen_range(0..EARTH_ORBIT_DURATION);
        let earth = Planets::make_planet(
            EARTH_ID, PlanetKind::Earth,
            EARTH_MASS, EARTH_SIZE,
            (0.0, 0.0),
            None,
            Some(Orbit {
                orbit_around: SUN_ID,
                radius: (1496.0, 1496.0),
                rotation: 0.0,
                total_ticks: EARTH_ORBIT_DURATION,
                tick_offset: earth_ticks_ellapsed,
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Beamout,
            bodies, colliders, &mut planets,
        );

        let moon = Planets::make_planet(
            MOON_ID, PlanetKind::Moon,
            EARTH_MASS / 35.0, EARTH_SIZE / 4.0,
            (0.0, 0.0),
            Some(PartKind::LandingThruster),
            Some(Orbit {
                orbit_around: EARTH_ID,
                radius: (100.0, 100.0),
                rotation: 0.0,
                total_ticks: MOON_ORBIT_DURATION,
                tick_offset: rand.gen_range(0..MOON_ORBIT_DURATION),
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Beamout,
            bodies, colliders, &mut planets,
        );

        let mars = Planets::make_planet(
            MARS_ID, PlanetKind::Mars,
            EARTH_MASS / 4.0, EARTH_SIZE / 2.0,
            (0.0, 0.0),
            Some(PartKind::Hub),
            Some(Orbit {
                orbit_around: SUN_ID,
                radius: (2279.0, 2279.0),
                rotation: 0.0,
                total_ticks: MARS_ORBIT_DURATION,
                tick_offset: rand.gen_range(0..MARS_ORBIT_DURATION),
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Dock,
            bodies, colliders, &mut planets,
        );

        let mercury = Planets::make_planet(
            MERCURY_ID, PlanetKind::Mercury,
            EARTH_MASS / 15.0, EARTH_SIZE * 0.38,
            (0.0, 0.0),
            Some(PartKind::SolarPanel),
            Some(Orbit {
                orbit_around: SUN_ID,
                radius: (579.0, 579.0),
                rotation: 0.0,
                total_ticks: MERCURY_ORBIT_DURATION,
                tick_offset: rand.gen_range(0..MERCURY_ORBIT_DURATION),
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Dock,
            bodies, colliders, &mut planets,
        );

        let jupiter = Planets::make_planet(
            JUPITER_ID, PlanetKind::Jupiter,
            EARTH_MASS * 11.5, EARTH_SIZE * 2.0,
            (0.0, 0.0),
            Some(PartKind::SuperThruster),
            Some(Orbit {
                orbit_around: SUN_ID,
                radius: (9000.0, 9000.0),
                rotation: 0.0,
                total_ticks: JUPITER_ORBIT_DURATION,
                tick_offset: rand.gen_range(0..JUPITER_ORBIT_DURATION),
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Dock,
            bodies, colliders, &mut planets,
        );

        let pluto = Planets::make_planet(
            PLUTO_ID, PlanetKind::Pluto,
            EARTH_MASS / 10.0, EARTH_SIZE / 4.0,
            (0.0, 0.0),
            Some(PartKind::LandingWheel),
            Some(Orbit {
                orbit_around: SUN_ID,
                radius: (20000.0, 25000.0),
                rotation: std::f32::consts::PI / 5.0,
                total_ticks: PLUTO_ORBIT_DURATION,
                tick_offset: rand.gen_range(0..PLUTO_ORBIT_DURATION),
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Dock,
            bodies, colliders, &mut planets,
        );

        let saturn = Planets::make_planet(
            SATURN_ID, PlanetKind::Saturn,
            EARTH_MASS * 6.75, EARTH_SIZE * 2.0,
            (0.0, 0.0),
            Some(PartKind::Thruster),
            Some(Orbit {
                orbit_around: SUN_ID,
                radius: (11750.0, 11750.0),
                rotation: 0.0,
                total_ticks: SATURN_ORBIT_DURATION,
                tick_offset: rand.gen_range(0..SATURN_ORBIT_DURATION),
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Dock,
            bodies, colliders, &mut planets,
        );

        let neptune = Planets::make_planet(
            NEPTUNE_ID, PlanetKind::Neptune,
            EARTH_MASS * 7.0, EARTH_SIZE * 1.5,
            (0.0, 0.0),
            Some(PartKind::PowerHub),
            Some(Orbit {
                orbit_around: SUN_ID,
                radius: (17250.0, 17250.0),
                rotation: 0.0,
                total_ticks: NEPTUNE_ORBIT_DURATION,
                tick_offset: rand.gen_range(0..NEPTUNE_ORBIT_DURATION),
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Dock,
            bodies, colliders, &mut planets,
        );

        let venus = Planets::make_planet(
            VENUS_ID, PlanetKind::Venus,
            EARTH_MASS * 1.3, EARTH_SIZE,
            (0.0, 0.0),
            Some(PartKind::EcoThruster),
            Some(Orbit {
                orbit_around: SUN_ID,
                radius: (1081.0, 1081.0),
                rotation: 0.0,
                total_ticks: VENUS_ORBIT_DURATION,
                tick_offset: rand.gen_range(0..VENUS_ORBIT_DURATION),
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Dock,
            bodies, colliders, &mut planets,
        );

        let uranus = Planets::make_planet(
            URANUS_ID, PlanetKind::Uranus,
            EARTH_MASS * 6.0, EARTH_SIZE * 2.0,
            (0.0, 0.0),
            Some(PartKind::HubThruster),
            Some(Orbit {
                orbit_around: SUN_ID,
                radius: (14500.0, 14500.0),
                rotation: 0.0,
                total_ticks: URANUS_ORBIT_DURATION,
                tick_offset: rand.gen_range(0..URANUS_ORBIT_DURATION),
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Dock,
            bodies, colliders, &mut planets,
        );

        /*let trade = Planets::make_planet(
            TRADE_ID, PlanetKind::Trade,
            EARTH_MASS, EARTH_SIZE * 0.75,
            (0.0, 0.0),
            None,
            Some(Orbit {
                orbit_around: SUN_ID,
                radius: (1496.0, 1496.0),
                rotation: 0.0,
                total_ticks: EARTH_ORBIT_DURATION,
                tick_offset: (earth_ticks_ellapsed + (EARTH_ORBIT_DURATION / 2)) % EARTH_ORBIT_DURATION,
                last_next_position: (0.0, 0.0),
                last_velocity: (0.0, 0.0),
            }),
            BeamoutKind::Beamout,
            bodies, colliders, &mut planets,
        );*/

        /*Planets {
            earth, moon, planet_material, mars, mercury, jupiter, /* pluto, */ saturn, neptune, venus, uranus, sun, /* trade, */
        }*/

        let planet_ids = planets.keys().cloned().collect();

        Planets { planets, earth_id: EARTH_ID, trade_id: TRADE_ID, sun_id: SUN_ID, planet_ids }
    }

    pub fn advance_orbits(&mut self, bodies: &mut RigidBodySet, tick_num: u32) {
        for id in &self.planet_ids {
            if let Some(orbit) = &self.planets[&id].orbit {
                let parent_planet = &self.planets[&orbit.orbit_around];
                let parent_pos = parent_planet.position;
                let parent_next_pos = if let Some(orbit) = &parent_planet.orbit { orbit.last_next_position } else { parent_pos };
                let planet = self.planets.get_mut(id).unwrap();
                let (pos, vel) = planet.orbit.as_mut().unwrap().advance(tick_num, parent_pos, parent_next_pos);
                //let pos = planet.orbit.as_mut().unwrap().advance(parent_pos);
                planet.position = pos;
                let body = &mut bodies[planet.body_handle];
                /*if *id == PLUTO_ID {
                    let error_x = pos.0 - body.position().translation.x;
                    let error_y = pos.1 - body.position().translation.y;
                    let error = (error_x * error_x + error_y * error_y).sqrt();
                    let velvel = (vel.0 * vel.0 + vel.1 * vel.1).sqrt();
                    println!("Pluto Error: {:.5} ({:.5}, {:.5}), {:.2}% of velocity", error, error_x, error_y, error / velvel * 100.0);
                }*/
                body.set_position(Isometry::new(Vector::new(pos.0, pos.1), 0.0), true);
                body.set_linvel(Vector::new(vel.0, vel.1), true);
                //body.set_next_kinematic_position(Isometry::new(Vector::new(pos.0, pos.1), 0.0));
            }
        }
    }
}

static mut NEXT_PLANET_ID: u8 = 1;
fn make_planet_id() -> u8 {
    unsafe { let id = NEXT_PLANET_ID; NEXT_PLANET_ID += 1; id }
}

pub struct CelestialObject {
    pub kind: PlanetKind,
    pub orbit: Option<Orbit>,
    pub radius: f32,
    pub mass: f32,
    pub cargo_upgrade: Option<super::parts::PartKind>,
    pub beamout: BeamoutKind,
    pub body_handle: RigidBodyHandle,
    pub position: (f32, f32),
}

pub struct Orbit {
    orbit_around: u8,
    radius: (f32, f32),
    rotation: f32,
    total_ticks: u32,
    tick_offset: u32,
    last_next_position: (f32, f32),
    last_velocity: (f32, f32),
}
const TICKS_PER_SECOND: f32 = crate::TICKS_PER_SECOND as f32;
impl Orbit {
    pub fn calculate_position_vel(&mut self, tick_num: u32, parent_pos: (f32, f32), parent_next_pos: (f32, f32)) -> ((f32, f32), (f32, f32)) {
    //pub fn calculate_position_vel(&mut self, parent_pos: (f32, f32)) -> (f32, f32) {
        let ticks_ellapsed = ((self.tick_offset + tick_num) % self.total_ticks) as f32;
        let total_ticks = self.total_ticks as f32;
        let radians = ticks_ellapsed / total_ticks * 2.0 * std::f32::consts::PI;
        let mut pos = (self.radius.0 * radians.cos(), self.radius.1 * radians.sin());
        if self.rotation != 0.0 { Self::my_rotate_point(&mut pos, self.rotation) };
        let pos = (pos.0 + parent_pos.0, pos.1 + parent_pos.1);
        self.last_next_position = pos;
        //pos

        let ticks_ellapsed = ticks_ellapsed + 1.0; // % total_ticks;
        let radians = ticks_ellapsed / total_ticks * 2.0 * std::f32::consts::PI;
        let mut next_pos = (self.radius.0 * radians.cos(), self.radius.1 * radians.sin());
        if self.rotation != 0.0 { Self::my_rotate_point(&mut next_pos, self.rotation) };
        let next_pos = (next_pos.0 + parent_next_pos.0, next_pos.1 + parent_next_pos.1);

        let vel = ((next_pos.0 - pos.0) * TICKS_PER_SECOND, (next_pos.1 - pos.1) * TICKS_PER_SECOND);
        self.last_velocity = vel;
        (pos, vel)
    }

    pub fn advance(&mut self, tick_num: u32, parent_pos: (f32, f32), parent_next_pos: (f32, f32)) -> ((f32, f32), (f32, f32)) {
    //pub fn advance(&mut self, parent_pos: (f32, f32)) -> (f32, f32) {
        /*self.ticks_ellapsed += 1;
        if self.ticks_ellapsed >= self.total_ticks { self.ticks_ellapsed = 0; }*/
        self.calculate_position_vel(tick_num, parent_pos, parent_next_pos)
    }

    fn my_rotate_point(point: &mut (f32, f32), radians: f32) {
        *point = crate::rotate_vector_with_angle(point.0, point.1, radians)        
    }

    pub fn init_messages(&self, my_id: u8) -> [ToClientMsg; 1] {
        [
            ToClientMsg::InitCelestialOrbit {
                id: my_id,
                orbit_around_body: self.orbit_around,
                orbit_radius: self.radius,
                orbit_rotation: self.rotation,
                orbit_total_ticks: self.total_ticks,
                orbit_tick_offset: self.tick_offset,
            }
        ]
    }
    
    pub fn last_velocity(&self) -> (f32, f32) { self.last_velocity }
}

#[derive(Copy, Clone)]
pub struct AmPlanet {
    pub id: u8
}
//use nphysics2d::utils::UserData;
/*impl UserData for AmPlanet {
    fn clone_boxed(&self) -> Box<dyn UserData> { Box::new(*self) }
    fn to_any(&self) -> Box<dyn std::any::Any + Send + Sync> { Box::new(*self) }
    fn as_any(&self) -> &dyn std::any::Any { self }    
}*/
