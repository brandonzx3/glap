use std::collections::{BTreeMap, BTreeSet};
use crate::PartOfPlayer;
use generational_arena::{Arena, Index};
use crate::codec::{ ToClientMsg, PartKind };
use std::ops::{Deref, DerefMut};
use rapier2d::dynamics::{BodyStatus, CCDSolver, JointSet, RigidBody, RigidBodyBuilder, RigidBodyHandle, RigidBodySet, IntegrationParameters, Joint, JointHandle, MassProperties, BallJoint, JointParams, IslandManager};
use rapier2d::geometry::{BroadPhase, NarrowPhase, ColliderSet, IntersectionEvent, ContactEvent, ColliderHandle, Collider};
use rapier2d::pipeline::{PhysicsPipeline, ChannelEventCollector};
use rapier2d::crossbeam::channel::{Sender as CSender, Receiver as CReceiver, unbounded as c_channel};
use crate::storage7573::Storage7573;
use crate::PlayerMeta;

pub mod planets;
pub mod parts;
use parts::{Part, AttachedPartFacing, RecursivePartDescription, CompactThrustMode, AttachmentPointDetails};
use planets::AmPlanet;

pub mod typedef {
    pub type MyUnits = f32;
    pub type PartHandle = generational_arena::Index;
    //pub type MyIsometry = nphysics2d::math::Isometry<MyUnits>;
    pub type Vector = rapier2d::na::Matrix2x1<f32>;
    pub type Isometry = rapier2d::math::Isometry<f32>;
    pub type UnitComplex = rapier2d::na::UnitComplex<f32>;
    pub type Point = rapier2d::math::Point<f32>;
}
use typedef::*;

//const PART_JOINT_MAX_TORQUE: f32 = 200.0;
const PART_JOINT_MAX_TORQUE: f32 = 30.0;
const PART_JOINT_MAX_FORCE: f32 = PART_JOINT_MAX_TORQUE * 3.5;

type PartsReverseLookup = BTreeMap<(u32, u32), Index>;
pub struct World {
    pub bodies: RigidBodySet,
    pub islands: IslandManager,
    pipeline: PhysicsPipeline,
    integration_parameters: IntegrationParameters,
    broad_phase: BroadPhase,
    narrow_phase: NarrowPhase,
    pub colliders: ColliderSet,
    pub joints: JointSet,
    ccd_solver: CCDSolver,
    steps_per_batch: u8,
    intersection_events: CReceiver<IntersectionEvent>,
    contact_events: CReceiver<ContactEvent>,
    event_collector: ChannelEventCollector,
    parts_reverse_lookup: PartsReverseLookup,
}
pub enum SimulationEvent {
    PlayerTouchPlanet { player: u16, part: PartHandle, planet: u8, },
    PlayerUntouchPlanet { player: u16, part: PartHandle, planet: u8 },
    PartsDetached( Vec<PartHandle> ),
}
pub struct SimulationTick<'a> {
    ticks_left: u8,
    simulation: &'a mut Simulation
}

pub fn make_local_point(reference: &Isometry, local: Point) -> Point {
    Point { coords: reference.transform_vector(&local.coords) } + &reference.translation.vector
}
pub fn apply_force_locally(body: &mut RigidBody, force: Vector, local: Point, wake_up: bool) {
    body.apply_force(body.position() * force, wake_up);
    let distance = body.mass_properties().local_com - local;
    //WANT ANGLE FROM DISTANCE TO FORCE
    //let theta = (((distance.x * force.x) + (distance.y * force.y)) / distance.magnitude() / force.magnitude()).acos();
    let mut theta = distance.angle(&force);
    let s_value = (distance.x * force.y) - (force.x * distance.y);
    if s_value > 0.0 { theta = -theta; };
    let torque = distance.magnitude() * force.magnitude() * theta.sin();
    body.apply_torque(torque, true);
}

impl World {
    pub fn new(step_time: f32, steps_per_batch: u8) -> World {
        /*mechanics.set_timestep(step_time);
        mechanics.integration_parameters.max_ccd_substeps = 5;*/
        let mut colliders = ColliderSet::new();

        let intersection_events = c_channel();
        let contact_events = c_channel();
        let event_collector = ChannelEventCollector::new(intersection_events.0, contact_events.0);
        let mut integration_parameters = IntegrationParameters::default();
        integration_parameters.dt = step_time / steps_per_batch as f32;
        integration_parameters.erp = 0.4;
        integration_parameters.max_position_iterations = 8;
        integration_parameters.max_velocity_iterations = 8;

        let simulation = World {
            pipeline: PhysicsPipeline::new(),
            integration_parameters,
            broad_phase: BroadPhase::new(),
            narrow_phase: NarrowPhase::new(),
            ccd_solver: CCDSolver::new(),

            steps_per_batch,
            bodies: RigidBodySet::new(),
            islands: IslandManager::new(),
            colliders,
            joints: JointSet::new(),
            event_collector,
            intersection_events: intersection_events.1,
            contact_events: contact_events.1,
            parts_reverse_lookup: BTreeMap::new(),
        };
        simulation
    }

    pub fn release_constraint(&mut self, constraint_id: JointHandle) {
        self.joints.remove(constraint_id, &mut self.islands, &mut self.bodies, true);
    }

    pub fn is_constraint_broken(&self, handle: JointHandle) -> bool {
        //self.joints.get(handle).map(|joint| joint.is_broken()).unwrap_or(true)
        false
    }
}
impl<'a> SimulationTick<'a> {
    pub fn new(simulation: &'a mut Simulation) -> SimulationTick {
        SimulationTick {
            ticks_left: simulation.world.steps_per_batch,
            simulation,
        }
    }
    pub fn needs_step(&self) -> bool { self.ticks_left > 0 }
    pub fn is_logic_step(&self) -> bool { self.ticks_left == 1 }
    pub fn step(&mut self, mut simulation_events: &mut Vec<SimulationEvent>) -> std::thread::Result<()> {
        self.ticks_left = self.ticks_left.checked_sub(1).expect("Stepped one step too far");
        let mut my_simulation_events = std::panic::AssertUnwindSafe(&mut simulation_events);
        let mut my_simulation = std::panic::AssertUnwindSafe(&mut self.simulation);
        std::panic::catch_unwind(move || my_simulation.simulate_step(&mut *my_simulation_events))?;
        if self.ticks_left == 0 {
            let mut my_simulation_events = std::panic::AssertUnwindSafe(&mut simulation_events);
            let mut my_simulation = std::panic::AssertUnwindSafe(&mut self.simulation);
            std::panic::catch_unwind(move || my_simulation.simulate_end(&mut *my_simulation_events))?;
        }
        Ok(())
    }
}
impl<'a> Deref for SimulationTick<'a> {
    type Target = Simulation;
    fn deref(&self) -> &Simulation { &self.simulation }
}
impl<'a> DerefMut for SimulationTick<'a> {
    fn deref_mut(&mut self) -> &mut Simulation { &mut self.simulation }
}

pub struct MouseDraggingStuff {
    part: PartHandle,
}

pub struct Simulation {
    pub world: World,
    parts: Arena<Part>,
    pub planets: planets::Planets,
    reference_point_body: RigidBodyHandle,
}

pub struct ClampAddress {
    pub parent: PartHandle,
    pub attachment_slot: usize,
    pub clamp_pos: Isometry,
    pub parent_visit_details: PartVisitDetails,
    pub attachment_details: AttachmentPointDetails,
}

impl Simulation {
    pub fn get_part(&self, index: PartHandle) -> &Part {
        self.parts.get(index).expect("Invalid PartHandle")
    }
    pub fn get_part_rigid(&self, index: PartHandle) -> &RigidBody {
        self.world.bodies.get(self.parts.get(index).expect("Invalid PartHandle").body_handle()).expect("Invalid RigidBodyHandle")
    }
    pub fn get_part_mut(&mut self, index: PartHandle) -> &mut Part {
        self.parts.get_mut(index).expect("Invalid PartHandle")
    }
    pub fn get_part_rigid_mut(&mut self, index: PartHandle) -> &mut RigidBody {
        self.world.bodies.get_mut(self.parts.get(index).expect("Invalid PartHandle").body_handle()).expect("Invalid RigidBodyHandle")
    }
    pub fn delete_parts_recursive(&mut self, index: PartHandle, removal_msgs: &mut Vec<ToClientMsg>) {
        self.parts.remove(index).map(|part| part.delete_recursive(self, removal_msgs));
    }
    pub fn split_part_and_world(&mut self, index: PartHandle) -> (&mut Part, &mut World) {
        (self.parts.get_mut(index).expect("Invalid PartHandle"), &mut self.world)
    }

    pub fn recurse_part<'a, F>(&'a self, part_handle: PartHandle, details: PartVisitDetails, func: &mut F)
    where F: FnMut(PartVisitHandle<'a>) {
        let part = self.get_part(part_handle);
        func(PartVisitHandle(self, part_handle, part, details));
        let attachment_dat = part.kind().attachment_locations();
        for (i, attachment) in part.attachments().iter().enumerate() {
            if let Some(attachment) = attachment {
                let true_facing = attachment_dat[i].map(|dat| dat.facing.compute_true_facing(details.true_facing)).unwrap_or(AttachedPartFacing::Up);
                let delta_rel_part = true_facing.delta_rel_part();
                self.recurse_part(**attachment, PartVisitDetails {
                    part_rel_x: details.part_rel_x + delta_rel_part.0,
                    part_rel_y: details.part_rel_y + delta_rel_part.1,
                    my_facing: attachment_dat[i].map(|dat| dat.facing).unwrap_or(AttachedPartFacing::Up),
                    true_facing,
                    attachment_dat: attachment_dat[i],
                }, func);
            }
        }
    }
    pub fn recurse_part_mut<'a, F>(&'a mut self, part_handle: PartHandle, details: PartVisitDetails, func: &mut F)
    where F: FnMut(PartVisitHandleMut<'_>) {
        func(PartVisitHandleMut(self, part_handle, details));
        let part = self.get_part(part_handle);
        let attachment_dat = part.kind().attachment_locations();
        for (i, attachment) in part.attachments().iter().map(|attachment| attachment.as_ref().map(|attach| **attach)).collect::<Vec<_>>().into_iter().enumerate() {
            if let Some(attachment) = attachment {
                let details = details.branch(attachment_dat[i]);
                self.recurse_part_mut(attachment, details, func);
            }
        }
    }
    pub fn recurse_part_with_return<'a, V, F>(&'a self, part_handle: PartHandle, details: PartVisitDetails, func: &mut F) -> Option<V>
    where F: FnMut(PartVisitHandle<'a>) -> Option<V> {
        let part = self.get_part(part_handle);
        let result = func(PartVisitHandle(self, part_handle, part, details));
        if result.is_some() { return result };
        let attachment_dat = part.kind().attachment_locations();
        for (i, attachment) in part.attachments().iter().enumerate() {
            if let Some(attachment) = attachment {
                let true_facing = attachment_dat[i].map(|dat| dat.facing.compute_true_facing(details.true_facing)).unwrap_or(AttachedPartFacing::Up);
                let delta_rel_part = true_facing.delta_rel_part();
                if let Some(result) = self.recurse_part_with_return(**attachment, PartVisitDetails {
                    part_rel_x: details.part_rel_x + delta_rel_part.0,
                    part_rel_y: details.part_rel_y + delta_rel_part.1,
                    my_facing: attachment_dat[i].map(|dat| dat.facing).unwrap_or(AttachedPartFacing::Up),
                    true_facing,
                    attachment_dat: attachment_dat[i],
                }, func) {
                    return Some(result)
                }
            }
        }
        return None;
    }
    pub fn recurse_part_mut_with_return<'a, V, F>(&'a mut self, part_handle: PartHandle, details: PartVisitDetails, func: &mut F) -> Option<V>
    where F: FnMut(PartVisitHandleMut<'_>) -> Option<V> {
        let result = func(PartVisitHandleMut(self, part_handle, details));
        if result.is_some() { return result };
        drop(result);
        let part = self.get_part_mut(part_handle);
        let attachment_dat = part.kind().attachment_locations();
        for (i, attachment) in part.attachments().iter().map(|attachment| attachment.as_ref().map(|attach| **attach)).collect::<Vec<_>>().into_iter().enumerate() {
            if let Some(attachment) = attachment {
                let true_facing = attachment_dat[i].map(|dat| dat.facing.compute_true_facing(details.true_facing)).unwrap_or(AttachedPartFacing::Up);
                let delta_rel_part = true_facing.delta_rel_part();
                if let Some(result) = self.recurse_part_mut_with_return(attachment, PartVisitDetails {
                    part_rel_x: details.part_rel_x + delta_rel_part.0,
                    part_rel_y: details.part_rel_y + delta_rel_part.1,
                    my_facing: attachment_dat[i].map(|dat| dat.facing).unwrap_or(AttachedPartFacing::Up),
                    true_facing,
                    attachment_dat: attachment_dat[i],
                }, func) {
                    return Some(result)
                }
            }
        }
        return None;
    }

    pub fn recursive_thrust_player(&mut self, core_handle: PartHandle, player: &mut PlayerMeta, subtract_fuel: bool) {
        if let Some(part) = self.parts.get(core_handle) {
            part.thrust_no_recurse(&mut player.power, player.thrust_forwards, player.thrust_backwards, player.thrust_clockwise, player.thrust_counterclockwise, subtract_fuel, &mut self.world.bodies);
            let attachments = [part.attachments()[0].as_ref().map(|e| **e), part.attachments()[1].as_ref().map(|e| **e), part.attachments()[2].as_ref().map(|e| **e), part.attachments()[3].as_ref().map(|e| **e)];
            for i in 0..4 {
                if let Some(attachment) = attachments[i] {
                    self.recursive_thrust_player(attachment, player, subtract_fuel);
                }
            }
        }
    }

    pub fn recursive_detach_one(&mut self, parent_handle: PartHandle, attachment_slot: usize, player: &mut Option<&mut crate::PlayerMeta>, parts_affected: &mut Vec<PartHandle>) {
        let (part, world) = self.split_part_and_world(parent_handle);
        if let Some(attachment_handle) = part.detach_part_player_agnostic(attachment_slot, world) {
            if let Some(player) = player {
                let (attached_part, world) = self.split_part_and_world(attachment_handle);
                attached_part.remove_from(*player, &mut world.colliders);
                parts_affected.push(attachment_handle);
            } else {
                parts_affected.push(attachment_handle);
            }
            self.recursive_detach_all(attachment_handle, player, parts_affected);                                
        }
    }
    pub fn recursive_detach_all(&mut self, parent_handle: PartHandle, player: &mut Option<&mut crate::PlayerMeta>, parts_affected: &mut Vec<PartHandle>) {
        let part = self.get_part_mut(parent_handle);
        let attach_spots = part.kind().attachment_locations();
        for i in 0..part.attachments().len() {
            if attach_spots[i].is_none() { continue };
            self.recursive_detach_one(parent_handle, i, player, parts_affected);
        }
    }
    pub fn remove_part_unchecked(&mut self, part_handle: PartHandle) -> Part {
        self.parts.remove(part_handle).expect("remove_part_unchecked")
    }

    pub fn new(step_time: f32, steps_per_batch: u8) -> Simulation {
        let mut world = World::new(step_time, steps_per_batch);
        let reference_point_body = RigidBodyBuilder::new_static().mass(0f32).build();
        let reference_point_body = world.bodies.insert(reference_point_body);
        let planets = planets::Planets::new(&mut world.bodies, &mut world.colliders);
        Simulation {
            parts: Arena::new(),
            planets,
            reference_point_body,
            world
        }
    }

    fn advance_orbits(&mut self, tick_num: u32) {
        self.planets.advance_orbits(&mut self.world.bodies, tick_num);
    }

    fn celestial_gravity(&mut self) {
        for (_part_handle, part) in self.parts.iter() {
            let part = &mut self.world.bodies[part.body_handle()];
            const GRAVITATION_CONSTANT: f32 = 1.0; //Lolrandom
            for body in self.planets.planets.values() {
                let distance: (f32, f32) = ((body.position.0 - part.position().translation.x),
                                            (body.position.1 - part.position().translation.y));
                let magnitude: f32 = part.mass() * body.mass
                                     / (distance.0.powf(2f32) + distance.1.powf(2f32));
                                     //* GRAVITATION_CONSTANT;
                if distance.0.abs() > distance.1.abs() {
                    part.apply_force(Vector::new(if distance.0 >= 0.0 { magnitude } else { -magnitude }, distance.1 / distance.0.abs() * magnitude), false);
                } else {
                    part.apply_force(Vector::new(distance.0 / distance.1.abs() * magnitude, if distance.1 >= 0.0 { magnitude } else { -magnitude }), false);
                }
            }
        }
    }

    fn reference_point_body(&self) -> RigidBodyHandle {
        self.reference_point_body
    }


    pub fn new_tick(&mut self, tick_num: u32) -> SimulationTick {
        self.simulate_start(tick_num);
        SimulationTick::new(self)
    }
    fn simulate_start(&mut self, tick_num: u32) {
        self.advance_orbits(tick_num);
    }
    fn simulate_step(&mut self, events: &mut Vec<SimulationEvent>) {
        const GRAVITYNT: Vector = Vector::new(0.0, 0.0);
        self.celestial_gravity();
        self.world.pipeline.step(
            &GRAVITYNT,
            &self.world.integration_parameters,
            &mut self.world.islands,
            &mut self.world.broad_phase,
            &mut self.world.narrow_phase,
            &mut self.world.bodies,
            &mut self.world.colliders,
            &mut self.world.joints,
            &mut self.world.ccd_solver,
            &(), &self.world.event_collector,
        );

        fn do_glue(collider1: ColliderHandle, collider2: ColliderHandle, sim: &mut Simulation, colliders: &ColliderSet) {
            let collider1 = &colliders[collider1];
            let collider2 = &colliders[collider2];
            let storage1 = Storage7573::from(collider1.user_data);
            let storage2 = Storage7573::from(collider2.user_data);
            let non_planet;
            let planet;
            if matches!(storage1, Storage7573::Planet(_id)) && !matches!(storage2, Storage7573::Planet(_id)) {
                non_planet = collider2;
                planet = &sim.planets.planets[&storage1.planet_id()];
            } else if matches!(storage2, Storage7573::Planet(_id)) && !matches!(storage1, Storage7573::Planet(_id)) {
                non_planet = collider1;
                planet = &sim.planets.planets[&storage2.planet_id()];
            } else {
                return;
            }
            let planet_vel = planet.orbit.as_ref().map(|orbit| orbit.last_velocity()).unwrap_or((0.0, 0.0));
            /*if let Storage7573::PartOfPlayer(player) = Storage7573::from(non_planet.user_data) {
                dbg!(player);
                dbg!(planet.kind);
                dbg!(planet_vel);
            }*/
            let body = sim.get_part_rigid_mut(*sim.world.parts_reverse_lookup.get(&non_planet.parent().unwrap().into_raw_parts()).unwrap());
            body.set_linvel(Vector::new(planet_vel.0, planet_vel.1), true);
        }

        while let Ok(intersection_event) = self.world.intersection_events.try_recv() {
            let handle1 = intersection_event.collider1;
            let handle2 = intersection_event.collider2;
            let planet: u8;
            let other: ColliderHandle;
            if let Storage7573::Planet(am_planet) = self.world.colliders.get(handle1).unwrap().user_data.into() {
                planet = am_planet; other = handle2;
            } else if let Storage7573::Planet(am_planet) = self.world.colliders.get(handle2).unwrap().user_data.into() {
                planet = am_planet; other = handle1;
            } else { continue; }
            let part_coll = self.world.colliders.get(other).unwrap();
            if let Storage7573::PartOfPlayer(player_id) = self.world.colliders[other].user_data.into() {
                if intersection_event.intersecting {
                    events.push(SimulationEvent::PlayerTouchPlanet{ player: player_id, part: *self.world.parts_reverse_lookup.get(&part_coll.parent().unwrap().into_raw_parts()).unwrap(), planet });
                } else {
                    events.push(SimulationEvent::PlayerUntouchPlanet{ player: player_id, part: *self.world.parts_reverse_lookup.get(&part_coll.parent().unwrap().into_raw_parts()).unwrap(), planet });
                }
            }
        }

        for contact_pair in self.world.narrow_phase.contact_pairs() {
            if !contact_pair.has_any_active_contact { continue };
            //do_glue(contact_pair.collider1, contact_pair.collider2, &mut self.world, &self.colliders);
        }

        let mut to_detach = Vec::new();
        for (handle, part) in &mut self.parts {
            for i in 0..4 {
                if part.kind().attachment_locations()[i].is_none() { continue };
                if let Some(attachment) = &part.attachments()[i] {
                    let joint = self.world.joints.get(attachment.connection).unwrap();
                    if let JointParams::FixedJoint(joint_params) = joint.params {
                        if joint_params.impulse.xy().magnitude().abs() >= PART_JOINT_MAX_FORCE || joint_params.impulse.z.abs() >= PART_JOINT_MAX_TORQUE {
                            to_detach.push((handle, i));
                        }
                    } else {
                        panic!("Fixed Jointn't");
                    }
                }
            }
        }
        let mut parts_affected = Vec::new();
        for (parent, i) in to_detach {
            self.recursive_detach_one(parent, i, &mut None, &mut parts_affected);
        }
        if !parts_affected.is_empty() { events.push(SimulationEvent::PartsDetached(parts_affected)) };
    }
    fn simulate_end(&mut self, events: &mut Vec<SimulationEvent>) {

    }

    pub fn inflate(&mut self, parts: &RecursivePartDescription, initial_location: Isometry) -> PartHandle {
        parts.inflate(self, initial_location)
    }

    pub fn equip_mouse_dragging(&mut self, part: PartHandle, core_velocity: Vector) -> MouseDraggingStuff {
        self.recurse_part_mut(part, Default::default(), &mut |mut handle| {
            let body = handle.self_rigid_mut();
            body.set_dominance_group(-5);
            body.set_linvel(core_velocity, true);
        });
        MouseDraggingStuff { part }
    }
    pub fn update_mouse_dragging(&mut self, stuff: &mut MouseDraggingStuff, target_x: f32, target_y: f32, mut core_velocity: Vector, clamp_targets: &[PartHandle]) -> Option<ClampAddress> {
        let root_pos = self.get_part_rigid(stuff.part).position().clone();
        let dx = target_x - root_pos.translation.x;
        let dy = target_y - root_pos.translation.y;
        let vx = dx * 6.0;
        let vy = dy * 6.0;
        core_velocity.x += vx;
        core_velocity.y += vy;
        self.recurse_part_mut(stuff.part, Default::default(), &mut |mut handle| {
            handle.self_rigid_mut().set_linvel(core_velocity, true);
        });

        for clamp_target in clamp_targets {
            let res = self.recurse_part_with_return(*clamp_target, Default::default(), &mut |handle| {
                let parent = handle.deref();
                let parent_body = handle.self_rigid();                
                let parent_pos = parent_body.position().clone();
                let attachments = parent.kind().attachment_locations();
                for (i, attachment) in parent.attachments().iter().enumerate() {
                    if attachment.is_none() {
                        if let Some(details) = attachments[i] {
                            let clamp_pos = parent_pos * Isometry::new(Vector::new(details.x, details.y), details.facing.part_rotation());
                            if (clamp_pos.translation.x - target_x).abs() <= 0.4 && (clamp_pos.translation.y - target_y).abs() <= 0.4 {
                                let my_true_facing = details.facing.compute_true_facing(handle.details().true_facing);
                                
                                return Some(ClampAddress {
                                    parent: handle.handle(), 
                                    attachment_slot: i,
                                    clamp_pos,
                                    parent_visit_details: handle.details().clone(),
                                    attachment_details: details
                                });
                            }
                        }
                    }
                }
                None
            });
            if let Some(res) = res {
                let clamp_vel = self.get_part_rigid(*clamp_target).linvel().clone();
                self.recurse_part_mut(stuff.part, res.parent_visit_details.branch(Some(res.attachment_details)), &mut |mut handle| {
                    let body = handle.self_rigid_mut();
                    let old_pos = body.position().clone();
                    body.set_position(
                        Isometry::new(
                            res.clamp_pos.transform_point(&root_pos.inverse_transform_point(&(old_pos.translation.vector.into()))).coords,
                            //Point::from(old_pos.translation.vector.clone()) / root_pos.translation.vector * res.clamp_pos.translation.vector,
                            old_pos.rotation.angle() - root_pos.rotation.angle() + res.clamp_pos.rotation.angle()
                        ),
                        true
                    );
                    body.set_linvel(clamp_vel, true);
                });
                return Some(res);
            }
        }
        return None
    }
    pub fn release_mouse_constraint(&mut self, drag_stuff: MouseDraggingStuff) {
        self.recurse_part_mut(drag_stuff.part, Default::default(), &mut |mut handle| {
            let body = handle.self_rigid_mut();
            body.set_dominance_group(0);
        });
    }

    pub fn attach_part_player_agnostic(&mut self, parent: PartHandle, child: PartHandle, attach_to_slot: usize) {
        let attachment = parts::PartAttachment::inflate(&self.parts[parent], &self.parts[child], child, attach_to_slot, &mut self.world);
        let (part, world) = self.split_part_and_world(parent);
        part.attach_part_player_agnostic(attach_to_slot, attachment, world);
    }

    pub fn detach_part_player_agnostic(&mut self, parent: PartHandle, detach_from_slot: usize) -> Option<PartHandle> {
        let (part, world) = self.split_part_and_world(parent);
        part.detach_part_player_agnostic(detach_from_slot, world)
    }
}

#[derive(Copy, Clone)]
pub struct PartVisitDetails {
    pub part_rel_x: i32,
    pub part_rel_y: i32,
    pub my_facing: AttachedPartFacing,
    pub true_facing: AttachedPartFacing,
    pub attachment_dat: Option<parts::AttachmentPointDetails>,
}
impl Default for PartVisitDetails {
    fn default() -> Self { PartVisitDetails {
        part_rel_x: 0,
        part_rel_y: 0,
        my_facing: AttachedPartFacing::Up,
        true_facing: AttachedPartFacing::Up,
        attachment_dat: None,
    } }
}
impl PartVisitDetails {
    pub fn with_true_facing(true_facing: AttachedPartFacing) -> Self {
        PartVisitDetails {
            true_facing,
            ..Self::default()
        }
    }
    pub fn branch(&self, attachment_details: Option<AttachmentPointDetails>) -> PartVisitDetails {
        let true_facing = attachment_details.map(|details| details.facing.compute_true_facing(self.true_facing)).unwrap_or(AttachedPartFacing::Up);
        let delta_rel_part = true_facing.delta_rel_part();
        let details = PartVisitDetails {
            part_rel_x: self.part_rel_x + delta_rel_part.0,
            part_rel_y: self.part_rel_y + delta_rel_part.1,
            my_facing: attachment_details.map(|details| details.facing).unwrap_or(AttachedPartFacing::Up),
            true_facing,
            attachment_dat: attachment_details,
        };
        details
    }
}

pub struct PartVisitHandle<'a> (&'a Simulation, PartHandle, &'a Part, PartVisitDetails);
impl<'a> PartVisitHandle<'a> {
    pub fn get_part(&self, handle: PartHandle) -> &Part { self.0.get_part(handle) }
    pub fn get_rigid(&self, handle: PartHandle) -> &RigidBody { self.0.get_part_rigid(handle) }
    pub fn self_rigid(&self) -> &RigidBody { self.get_rigid(self.1) }
    pub fn handle(&self) -> PartHandle { self.1 }
    pub fn details(&self) -> &PartVisitDetails { &self.3 }
    pub fn world_unchecked(&self) -> &World { &self.0.world }
}
impl<'a> Deref for PartVisitHandle<'a> {
    type Target = Part;
    fn deref(&self) -> &Part { self.2 }
}
pub struct PartVisitHandleMut<'a> (&'a mut Simulation, PartHandle, PartVisitDetails);
impl<'a> PartVisitHandleMut<'a> {
    pub fn get_part(&self, handle: PartHandle) -> &Part { self.0.get_part(handle) }
    pub fn get_rigid(&self, handle: PartHandle) -> &RigidBody { self.0.get_part_rigid(handle) }
    pub fn get_part_mut(&mut self, handle: PartHandle) -> &mut Part { self.0.get_part_mut(handle) }
    pub fn get_rigid_mut(&mut self, handle: PartHandle) -> &mut RigidBody { self.0.get_part_rigid_mut(handle) }
    pub fn self_rigid(&self) -> &RigidBody { self.get_rigid(self.1) }
    pub fn self_rigid_mut(&mut self) -> &mut RigidBody { self.get_rigid_mut(self.1) }
    pub fn handle(&self) -> PartHandle { self.1 }
    pub fn details(&self) -> &PartVisitDetails { &self.2 }
    pub fn world_unchecked(&self) -> &World { &self.0.world }
    pub fn world_mut_unchecked(&mut self) -> &mut World { &mut self.0.world }
    pub fn world_part_unchecked(&mut self) -> (&mut Part, &mut World) { (self.0.parts.get_mut(self.1).expect("Handle Visit Partn't lol"), &mut self.0.world) }
}
impl<'a> Deref for PartVisitHandleMut<'a> {
    type Target = Part;
    fn deref(&self) -> &Part { self.get_part(self.1) }
}
impl<'a> DerefMut for PartVisitHandleMut<'a> {
    fn deref_mut(&mut self) -> &mut Part { self.get_part_mut(self.1) }
}
