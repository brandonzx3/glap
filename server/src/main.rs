#[macro_use] extern crate lazy_static;
use async_std::prelude::*;
use std::net::SocketAddr;
use futures::{FutureExt, StreamExt};
use std::pin::Pin;
use std::collections::{BTreeMap, BTreeSet, VecDeque};
use std::task::Poll;
use rand::Rng;
use world::typedef::*;
use world::parts::{Part, AttachedPartFacing};
use std::sync::Arc;
use std::any::Any;
use async_std::channel::{Sender, Receiver, bounded as channel};
use async_std::sync::Mutex;
use std::sync::atomic::{AtomicBool, Ordering as AtomicOrdering};

pub mod world;
pub mod codec;
pub mod session;
pub mod beamout;
pub mod storage7573;
use codec::*;
use session::ToSerializerEvent;

use world::parts::{RecursivePartDescription, PartKind};

pub const TICKS_PER_SECOND: u8 = 20;
pub const DEFAULT_PART_DECAY_TICKS: u16 = TICKS_PER_SECOND as u16 * 90;

static mut EMERGENCY_STOP: AtomicBool = AtomicBool::new(false);
pub fn is_emergency_stop() -> bool { unsafe { EMERGENCY_STOP.load(AtomicOrdering::Acquire) } }

#[derive(Clone)]
pub struct ApiDat { prefix: String, beamout: String, beamin: String, password: String }

#[async_std::main]
async fn main() {
    println!("======BEGIN BUILD INFO\n{}\n======END BUILD INFO", env!("build_info"));
    let server_port = if let Ok(port) = std::env::var("PORT") { port.parse::<u16>().unwrap_or(8081) } else { 8081 };
    let listener = async_std::net::TcpListener::bind(SocketAddr::new(std::net::IpAddr::V4(std::net::Ipv4Addr::UNSPECIFIED), server_port)).await.expect(&format!("Failed to bind to port {}", server_port));

    let api = std::env::var("API").ok().map(|prefix| ApiDat {
        prefix: prefix.clone(),
        beamout: prefix.clone() + "/user/^^^^/beamout",
        beamin: prefix.clone() + "/session/^^^^/beamin",
        password: std::env::var("API_PASSWORD").unwrap_or(String::with_capacity(0)),
    });

    let api = if let Some(api) = api {
        let ping_addr = api.prefix.clone() + "/ping";
        println!("Pinging API at ${}", ping_addr);
        let res = surf::get(ping_addr).await;
        if let Ok(mut res) = res {
            if res.status().is_success() && res.body_string().await.map(|body| body == "PONG" ).unwrap_or(false) { println!("API Ping success"); Some(api) }
            else { eprintln!("API Ping Failed"); None }
        } else { eprintln!("API Ping Failed Badly"); None }
    } else { println!("No API"); None };

    let api = api.map(|api| Arc::new(api));

    let (to_game, to_me) = channel::<session::ToGameEvent>(1024);
    let (to_serializer, to_me_serializer) = channel::<Vec<session::ToSerializerEvent>>(256);
    let suspended_players = Arc::new(Mutex::new(VecDeque::new()));
    println!("Hello from game task");
    let _incoming_connection_acceptor = async_std::task::Builder::new()
        .name("incoming_connection_acceptor".to_string())
        .spawn(session::incoming_connection_acceptor(listener, to_game.clone(), to_serializer.clone(), api.clone(), suspended_players.clone()));
    let _serializer = async_std::task::Builder::new()
        .name("serializer".to_string())
        .spawn(session::serializer(to_me_serializer, to_game.clone(), suspended_players.clone(), to_serializer.clone()));

    const TIMESTEP: f32 = 1.0/(TICKS_PER_SECOND as f32);
    let ticker = async_std::stream::interval(std::time::Duration::from_secs_f32(TIMESTEP));
    let mut sim = world::Simulation::new(TIMESTEP, 3);

    let mut players: BTreeMap<u16, PlayerMeta> = BTreeMap::new();
    let mut free_parts: BTreeMap<u16, FreePart> = BTreeMap::new();
    const MAX_EARTH_CARGOS: u8 = 20; const TICKS_PER_EARTH_CARGO_SPAWN: u8 = TICKS_PER_SECOND * 4;
    let mut earth_cargos: u8 = 0; let mut ticks_til_earth_cargo_spawn: u8 = TICKS_PER_EARTH_CARGO_SPAWN;
    let mut rand = rand::thread_rng();

    let signals = signal_hook_async_std::Signals::new(&[signal_hook::consts::SIGQUIT, signal_hook::consts::SIGTERM, signal_hook::consts::SIGINT]).expect("Failed to do signals");

    struct EventSource {
        pub inbound: async_std::channel::Receiver<session::ToGameEvent>,
        pub ticker: async_std::stream::Interval,
        pub signals: signal_hook_async_std::Signals,
    }
    enum Event {
        InboundEvent(session::ToGameEvent),
        Simulate,
        EmergencyStop
    }
    impl Stream for EventSource {
        type Item = Event;
        fn poll_next(mut self: Pin<&mut Self>, ctx: &mut std::task::Context) -> Poll<Option<Event>> {
            if let Poll::Ready(Some(_)) = self.signals.poll_next_unpin(ctx) { return Poll::Ready(Some(Event::EmergencyStop)); }
            if let Poll::Ready(Some(_)) = self.ticker.poll_next_unpin(ctx) { return Poll::Ready(Some(Event::Simulate)); }
            match self.inbound.poll_next_unpin(ctx) {
                Poll::Ready(Some(event)) => return Poll::Ready(Some(Event::InboundEvent(event))),
                Poll::Ready(None) => return Poll::Ready(None),
                Poll::Pending => ()
            };
            Poll::Pending
        }
    }
    let mut event_source = EventSource { inbound: to_me, ticker, signals };
    let mut simulation_events = Vec::new();
    const TICKS_PER_CARGO_UPGRADE: u8 = TICKS_PER_SECOND;

    let mut ticks_til_power_regen = 5u8;
    let mut tick_num: u32 = 0;

    while let Some(event) = event_source.next().await {
        use session::ToGameEvent::*;
        use session::ToSerializerEvent as ToSerializer;
        let mut outbound_events = Vec::new();
        match event {
            Event::Simulate => {
                tick_num += 1;

                let mut to_delete: Vec<u16> = Vec::new();
                for (part_handle, meta) in free_parts.iter_mut() {
                    match meta {
                        FreePart::Decaying(_part, ticks) => {
                            *ticks -= 1;
                            if *ticks < 1 { to_delete.push(*part_handle); }
                        },
                        FreePart::EarthCargo(part, ticks) => {
                            *ticks -= 1;
                            if *ticks < 1 {
                                let earth = &sim.planets.planets[&sim.planets.earth_id];
                                let spawn_radius = earth.radius * 1.25 + 1.0;
                                let earth_position = sim.world.bodies[earth.body_handle].position().translation;
                                let earth_position = sim.world.bodies[sim.planets.planets[&sim.planets.earth_id].body_handle].position().translation;
                                let body = sim.get_part_rigid_mut(*part);
                                let spawn_degrees: f32 = rand.gen::<f32>() * std::f32::consts::PI * 2.0;
                                body.set_position(Isometry::new(Vector::new(spawn_degrees.sin() * spawn_radius + earth_position.x, spawn_degrees.cos() * spawn_radius + earth_position.y), 0.0), true); // spawn_degrees));
                                //use nphysics2d::object::Body;
                                //body.apply_force(0, &nphysics2d::math::Force::zero(), nphysics2d::math::ForceType::Force, true);
                                *ticks = TICKS_PER_SECOND as u16 * 60;
                            }
                        },
                        FreePart::Grabbed(_part) => (),
                        FreePart::PlaceholderLol => panic!(),
                    }
                }
                let mut removal_messages = Vec::new();
                for to_delete in to_delete {
                    let meta = free_parts.remove(&to_delete).unwrap();
                    sim.delete_parts_recursive(*meta, &mut removal_messages);
                }
                outbound_events.extend(removal_messages.into_iter().map(|msg| ToSerializer::Broadcast(msg)));
                if earth_cargos < MAX_EARTH_CARGOS {
                    ticks_til_earth_cargo_spawn -= 1;
                    if ticks_til_earth_cargo_spawn == 0 {
                        ticks_til_earth_cargo_spawn = TICKS_PER_EARTH_CARGO_SPAWN;
                        earth_cargos += 1; 
                        let earth = &sim.planets.planets[&sim.planets.earth_id];
                        let spawn_radius = earth.radius * 1.25 + 1.0;
                        let earth_position = sim.world.bodies[earth.body_handle].position().translation;
                        let spawn_degrees: f32 = rand.gen::<f32>() * std::f32::consts::PI * 2.0;
                        let spawn_pos = Isometry::new(Vector::new(spawn_degrees.sin() * spawn_radius + earth_position.x, spawn_degrees.cos() * spawn_radius + earth_position.y), 0.0);
                        let part_handle = {
                            let mut part = RecursivePartDescription::from(PartKind::Cargo);
                            part.is_solid = true;
                            part.inflate(&mut sim, spawn_pos)
                        };
                        let part = sim.get_part(part_handle);
                        let part_id = part.id();
                        free_parts.insert(part_id, FreePart::EarthCargo(part_handle, TICKS_PER_SECOND as u16 * 60));
                        outbound_events.push(ToSerializer::Broadcast(part.add_msg()));
                        outbound_events.push(ToSerializer::Broadcast(part.move_msg(&sim.world)));
                        outbound_events.push(ToSerializer::Broadcast(part.update_meta_msg()));
                    }
                }
                ticks_til_power_regen -= 1;
                let is_power_regen_tick;
                if ticks_til_power_regen == 0 { ticks_til_power_regen = 5; is_power_regen_tick = true; }
                else { is_power_regen_tick = false; }
                for (id, player) in &mut players {
                    if is_power_regen_tick {
                        player.power += player.power_regen_per_5_ticks;
                        if player.power > player.max_power { player.power = player.max_power; };
                    };
                    if let Some((part_id, constraint, x, y)) = &mut player.grabbed_part {
                        let core = sim.get_part_rigid(player.core);
                        let position = core.position().translation;
                        let linvel = core.linvel().clone();
                        sim.update_mouse_dragging(constraint, *x + position.x, *y + position.y, linvel, &[player.core]);
                    }
                    if let Some(planet_id) = player.touching_planet {
                        player.ticks_til_cargo_transform -= 1;
                        if player.ticks_til_cargo_transform < 1 {
                            player.ticks_til_cargo_transform = TICKS_PER_CARGO_UPGRADE;
                            if let Some(upgrade_into) = sim.planets.planets[&planet_id].cargo_upgrade {
                                let core = sim.get_part(player.core);
                                if let Some((parent_part_handle, slot)) = core.find_cargo_recursive(&sim) {
                                    let parent_part_handle = parent_part_handle.unwrap_or(player.core);
                                    let old_part_handle = sim.detach_part_player_agnostic(parent_part_handle, slot).expect("Upgrade detachn't maybe?");
                                    let old_part = sim.remove_part_unchecked(old_part_handle);
                                    outbound_events.push(ToSerializer::Broadcast(old_part.remove_msg()));
                                    if player.parts_touching_planet.remove(&old_part_handle) {
                                        if player.parts_touching_planet.is_empty() { 
                                            player.touching_planet = None;
                                            player.beamout_status = BeamoutKind::None;
                                        }
                                    }
                                    let new_part_handle = old_part.mutate(upgrade_into, &mut Some(player), &mut sim);
                                    sim.attach_part_player_agnostic(parent_part_handle, new_part_handle, slot);
                                    sim.recurse_part(new_part_handle, Default::default(), &mut |new_part| {
                                        outbound_events.push(ToSerializer::Broadcast(new_part.add_msg()));
                                        outbound_events.push(ToSerializer::Broadcast(new_part.move_msg(&sim.world)));
                                        outbound_events.push(ToSerializer::Broadcast(new_part.update_meta_msg()));
                                    });
                                    outbound_events.push(ToSerializer::Message(*id, player.update_my_meta()));
                                }   
                            }
                        }
                    }
                }

                let mut tick = sim.new_tick(tick_num);
                const SHOULD_THRUST: bool = true;
                for (_player_id, player) in &mut players {
                    if player.power > 0 {
                        tick.recursive_thrust_player(player.core, player, SHOULD_THRUST);
                        if player.power < 1 {
                            player.thrust_backwards = false; player.thrust_forwards = false; player.thrust_clockwise = false; player.thrust_counterclockwise = false;
                            outbound_events.push(ToSerializer::Broadcast(player.update_meta_msg()));
                        }
                    }
                };
                while tick.needs_step() {
                    //let is_logic_step = tick.is_logic_step();
                    if let Err(err) = tick.step(&mut simulation_events) {
                        eprintln!("{:?}", err);
                        emergency_stop(&players, &sim, &api).await;
                        break;
                    }
                }

                let mut deletion_messages = Vec::new();
                for event in simulation_events.drain(..) {
                    use world::SimulationEvent::*;
                    match event {
                        PlayerTouchPlanet{ player, planet, part } => {
                            let player_id = player;
                            if let Some(player) = players.get_mut(&player) {
                                let part_meta = sim.get_part(part);
                                if part_meta.part_of_player() == Some(player.id) {
                                    if planet == sim.planets.sun_id {
                                        //Kill player
                                        outbound_events.push(ToSerializer::Broadcast(codec::ToClientMsg::IncinerationAnimation{ player_id }));
                                        let my_to_serializer = to_serializer.clone();
                                        let player = players.remove(&player_id).unwrap();
                                        let _deflated_ship = sim.get_part(player.core).deflate(&sim);
                                        //Don't need to send deletion messages since the client will
                                        //take care of IncinerationAnimation
                                        let mut trash_dump_messages_lol = Vec::new();
                                        sim.delete_parts_recursive(player.core, &mut trash_dump_messages_lol);
                                        async_std::task::spawn(async move {
                                            futures_timer::Delay::new(std::time::Duration::from_millis(2500)).await;
                                            my_to_serializer.send(vec![ ToSerializer::DeleteWriter(player_id) ]).await;
                                        });
                                    } else {
                                        player.touching_planet = Some(planet);
                                        player.beamout_status = sim.planets.planets[&planet].beamout;
                                        player.ticks_til_cargo_transform = TICKS_PER_SECOND;
                                        player.parts_touching_planet.insert(part);
                                        player.power = player.max_power;
                                        outbound_events.push(ToSerializer::Message(player_id, player.update_my_meta()));
                                    }
                                }
                            } else if planet == sim.planets.sun_id {
                                sim.delete_parts_recursive(part, &mut deletion_messages);
                            }
                        },
                        PlayerUntouchPlanet{ player, planet, part } => {
                            let player_id = player;
                            if let Some(player) = players.get_mut(&player) {
                                if player.parts_touching_planet.remove(&part) {
                                    if player.parts_touching_planet.is_empty() { 
                                        player.touching_planet = None;
                                        player.beamout_status = BeamoutKind::None;
                                        outbound_events.push(ToSerializer::Message(player_id, player.update_my_meta()));
                                    }
                                }
                            }
                        },
                        PartsDetached(affected_parts) => {
                            broken_part_detach_finish(affected_parts, &mut sim, &mut free_parts, &mut players, &mut outbound_events);
                        },
                    }
                }
                outbound_events.extend(deletion_messages.into_iter().map(|msg| ToSerializer::Broadcast(msg)));

                let mut free_part_messages = Vec::new();
                for part in free_parts.values() {
                    sim.recurse_part(**part, Default::default(), &mut |part| {
                        let body = part.get_rigid(part.handle());
                        let position = body.position();
                        free_part_messages.push(session::WorldUpdatePartMove {
                            id: part.id(),
                            x: position.translation.x, y: position.translation.y,
                            rot_cos: position.rotation.re, rot_sin: position.rotation.im
                        });
                    });
                }
                outbound_events.push(ToSerializer::WorldUpdate(
                    {
                        let mut out = BTreeMap::new();
                        for (id, player) in &players {
                            let mut parts = Vec::new();
                            let part = sim.get_part(player.core);
                            let vel = sim.get_part_rigid(player.core).linvel();
                            part.physics_update_msg(&sim, &mut parts);
                            out.insert(*id, ((parts[0].x, parts[0].y), (vel.x, vel.y), parts, ToClientMsg::PostSimulationTick{ your_power: player.power }));
                        }
                        out
                    },
                    free_part_messages
                ));

                outbound_events.push(ToSerializer::Broadcast(ToClientMsg::Flow { tick: tick_num }));
            },


            Event::InboundEvent(PlayerQuit { id }) => {
                outbound_events.push(ToSerializer::Broadcast(codec::ToClientMsg::RemovePlayer{ id }));
                if let Some(mut player) = players.remove(&id) {
                    println!("Player {} disconnected with id {}", player.name, id);
                    let mut affected_parts = Vec::new();
                    sim.recursive_detach_all(player.core, &mut Some(&mut player), &mut affected_parts);
                    for handle in affected_parts {
                        let part = sim.get_part(handle);
                        outbound_events.push(ToSerializer::Broadcast(part.update_meta_msg()));
                        free_parts.insert(part.id(), FreePart::Decaying(handle, DEFAULT_PART_DECAY_TICKS));
                    }
                    let mut deletion_messages = Vec::new();
                    sim.delete_parts_recursive(player.core, &mut deletion_messages);
                    outbound_events.extend(deletion_messages.into_iter().map(|msg| ToSerializer::Broadcast(msg)));
                    if let Some((part_id, constraint_id, _, _)) = player.grabbed_part {
                        if let Some(part) = free_parts.get_mut(&part_id) {
                            part.become_decaying();
                            sim.release_mouse_constraint(constraint_id);
                        }
                    }
                    outbound_events.push(ToSerializer::Broadcast(ToClientMsg::ChatMessage{ username: String::from("Server"), msg: player.name.clone() + " left the game", color: "#87de87".to_string() }));
                } 
            },

            Event::InboundEvent(PlayerSuspend { id, ref_handle }) => {
                if let Some(player) = players.get_mut(&id) {
                    println!("Player {} suspended with id {}", player.name, id);
                    player.thrust_forwards = false;
                    player.thrust_backwards = false;
                    player.thrust_counterclockwise = false;
                    player.thrust_clockwise = false;
                    if let Some((id, constraint, _, _)) = std::mem::replace(&mut player.grabbed_part, None) {
                        sim.release_mouse_constraint(constraint);
                        free_parts.get_mut(&id).unwrap().become_decaying();
                    }
                    outbound_events.push(ToSerializer::Broadcast(player.update_meta_msg()));
                    outbound_events.push(ToSerializer::Broadcast(ToClientMsg::ChatMessage { username: "Server".to_owned(), msg: format!("{} has disconnected", player.name), color: "#87de87".to_string() }));

                    let suspended_player = Arc::new(session::SuspendedPlayer { id, session: ref_handle });
                    let my_suspended_player = Arc::downgrade(&suspended_player);
                    suspended_players.lock().await.push_back(suspended_player);
                    let my_suspended_players = suspended_players.clone();
                    let my_to_game = to_game.clone();
                    async_std::task::spawn(async move {
                        async_std::task::sleep(std::time::Duration::from_secs(70)).await;
                        let mut my_suspended_players = my_suspended_players.lock().await;
                        if let Some(my_suspended_player) = my_suspended_player.upgrade() {
                            for i in 0..my_suspended_players.len() {
                                if Arc::ptr_eq(&my_suspended_player, &my_suspended_players[i]) {
                                    my_suspended_players.remove(i);
                                    break;
                                }
                            }
                            my_to_game.send(PlayerQuit { id }).await;
                        }
                        drop(my_suspended_players);
                    });
                } else {
                    println!("FAILED to suspend player {}", id);
                }
            },
            Event::InboundEvent(PlayerReconnect { id }) => {
                if let Some(player) = players.get(&id) {
                    println!("Player {} reconnected with id {}", player.name, id);
                    outbound_events.push(ToSerializer::Message(id, ToClientMsg::HandshakeAccepted{ id, core_id: sim.get_part(player.core).id(), can_beamout: player.beamout_token.is_some() }));
                    outbound_events.push(ToSerializer::Broadcast(ToClientMsg::ChatMessage { username: "Server".to_owned(), msg: format!("{} has reconnected", player.name), color: "#87de87".to_string() }));
                } else {
                    println!("FAILED to reconnect player {}", id);
                    outbound_events.push(ToSerializer::DeleteWriter(id));
                }
            },
            
            Event::InboundEvent(NewPlayer{ id, name, parts, beamout_token, beamin_to_planet }) => { 
                println!("New Player {} with id {}", name, id);
                let earth_id = beamin_to_planet.unwrap_or(sim.planets.earth_id);
                let earth_position = sim.world.bodies[sim.planets.planets[&earth_id].body_handle].position().translation;
                let spawn_vel = sim.world.bodies[sim.planets.planets[&earth_id].body_handle].linvel().clone();
                let earth_radius = sim.planets.planets[&earth_id].radius;
                use rand::Rng;

                let spawn_degrees: f32 = rand.gen::<f32>() * std::f32::consts::PI * 2.0;
                let core_handle = sim.inflate(&parts, Isometry::new(Vector::new(0.0,0.0), spawn_degrees - std::f32::consts::FRAC_PI_2));
                let mut max_extent: i32 = 1;
                sim.recurse_part(core_handle, Default::default(), &mut |handle: world::PartVisitHandle| max_extent = max_extent.min(handle.details().part_rel_y));
                let max_extent = max_extent as f32 / 3.0;
                let spawn_radius: f32 = earth_radius * 1.25 + 1.0 + max_extent.abs();
                let spawn_center = (Vector::new(spawn_degrees.cos(), spawn_degrees.sin()) * spawn_radius) + earth_position.vector;
                sim.recurse_part_mut(core_handle, Default::default(), &mut |mut handle: world::PartVisitHandleMut| {
                    let part = handle.self_rigid_mut();
                    let new_pos = Isometry::new(
                        part.position().translation.vector.clone() + spawn_center,
                        part.position().rotation.angle()
                    );
                    part.set_position(new_pos, true);
                    part.set_linvel(spawn_vel.clone(), true);
                });

                let core = sim.get_part_mut(core_handle);

                outbound_events.push(ToSerializer::Message(id, ToClientMsg::HandshakeAccepted{ id, core_id: core.id(), can_beamout: beamout_token.is_some() }));
                outbound_events.push(ToSerializer::Broadcast(codec::ToClientMsg::AddPlayer { id, name: name.clone(), core_id: core.id() }));
                
                let mut player = PlayerMeta::new(id, core_handle, name.clone(), beamout_token);
                sim.recurse_part_mut(core_handle, Default::default(), &mut |mut handle| {
                    let (part, world) = handle.world_part_unchecked();
                    part.join_to(&mut player, &mut world.colliders);
                    let part = &handle;
                    outbound_events.push(ToSerializer::Broadcast(part.add_msg()));
                    outbound_events.push(ToSerializer::Broadcast(part.move_msg(&handle.world_unchecked())));
                    outbound_events.push(ToSerializer::Broadcast(part.update_meta_msg()));
                });
                player.power = player.max_power;

                outbound_events.push(ToSerializer::Message(id, player.update_my_meta()));
                players.insert(id, player);
                outbound_events.push(ToSerializer::Broadcast(ToClientMsg::ChatMessage{ username: String::from("Server"), msg: name + " beamed in", color: "#87de87".to_string()  }));
            },
            Event::InboundEvent(SendEntireWorld{ to_player, send_self }) => {
                //Send over celestial object locations
                for (id, planet) in sim.planets.planets.iter() {
                    let position = sim.world.bodies[planet.body_handle].position().translation;
                    outbound_events.push(ToSerializer::Message(to_player, ToClientMsg::AddCelestialObject {
                        id: *id, radius: planet.radius, position: (position.x, position.y),
                        kind: planet.kind,
                    }));
                    if let Some(orbit) = &planet.orbit {
                        for msg in orbit.init_messages(*id) {
                            outbound_events.push(ToSerializer::Message(to_player, msg));
                        }
                    }
                }

                for (_id, part) in &free_parts { sim.recurse_part(**part, Default::default(), &mut |handle: world::PartVisitHandle| {
                    let part = &handle;
                    outbound_events.push(ToSerializer::Message(to_player, part.add_msg()));
                    outbound_events.push(ToSerializer::Message(to_player, part.move_msg(&sim.world)));
                    outbound_events.push(ToSerializer::Message(to_player, part.update_meta_msg()));
                }); }
                for (other_id, other_player) in &players {
                    if !send_self && *other_id == to_player { continue };
                    let other_core = sim.get_part(other_player.core);
                    outbound_events.push(ToSerializer::Message(to_player, codec::ToClientMsg::AddPlayer{ id: *other_id, name: other_player.name.clone(), core_id: other_core.id() }));
                    sim.recurse_part(other_player.core, Default::default(), &mut |handle: world::PartVisitHandle| {
                        let part = &handle;
                        outbound_events.push(ToSerializer::Message(to_player, part.add_msg()));
                        outbound_events.push(ToSerializer::Message(to_player, part.move_msg(&sim.world)));
                        outbound_events.push(ToSerializer::Message(to_player, part.update_meta_msg()));
                    });
                }
                if send_self {
                    if let Some(player) = players.get(&to_player) {
                        outbound_events.push(ToSerializer::Message(to_player, player.update_my_meta()));
                    }
                }

                outbound_events.push(ToSerializer::Message(to_player, codec::ToClientMsg::ChatMessage{ color: "#87de87".to_owned(), username: "Server".to_owned(), msg: format!("There are {} players online", players.len()) }));
            },

            Event::InboundEvent(PlayerMessage{ id, msg }) => {
                match msg {
                    ToServerMsg::SetThrusters{ forward, backward, clockwise, counter_clockwise } => {
                        if let Some(player) = players.get_mut(&id) {
                            if player.power > 0 {
                                player.thrust_forwards = forward;
                                player.thrust_backwards = backward;
                                player.thrust_clockwise = clockwise;
                                player.thrust_counterclockwise = counter_clockwise;
                                outbound_events.push(ToSerializer::Broadcast(codec::ToClientMsg::UpdatePlayerMeta {
                                    id,
                                    thrust_forward: forward, thrust_backward: backward, thrust_clockwise: clockwise, thrust_counter_clockwise: counter_clockwise,
                                    grabed_part: player.grabbed_part.as_ref().map(|(id, _, _, _)| *id)
                                }));
                            };
                        }
                    },

                    ToServerMsg::CommitGrab{ grabbed_id, x, y } => {
                        if let Some(player_meta) = players.get_mut(&id) {
                            if player_meta.grabbed_part.is_none() {
                                let core_velocity = sim.get_part_rigid(player_meta.core).linvel().clone();
                                if let Some(free_part) = free_parts.get_mut(&grabbed_id) {
                                    if let FreePart::Decaying(part, _) | FreePart::EarthCargo(part, _) = &free_part {
                                        player_meta.grabbed_part = Some((grabbed_id, sim.equip_mouse_dragging(*part, core_velocity), x, y));
                                        outbound_events.push(ToSerializer::Broadcast(sim.get_part(*part).update_meta_msg()));
                                        outbound_events.push(ToSerializer::Broadcast(player_meta.update_meta_msg()));
                                        free_part.become_grabbed(&mut earth_cargos);
                                    }
                                } else {
                                    if let Some(part_handle) = sim.recurse_part_mut_with_return(player_meta.core, Default::default(), &mut |mut handle| {
                                        for (i, attachment) in (*handle).attachments().iter().enumerate() {
                                            if let Some(attachment) = attachment {
                                                if handle.get_part(**attachment).id() == grabbed_id {
                                                    let (part, world) = handle.world_part_unchecked();
                                                    return part.detach_part_player_agnostic(i, world)
                                                };
                                            }
                                        };
                                        None
                                    }) {
                                        let (part, world) = sim.split_part_and_world(part_handle);
                                        part.remove_from(player_meta, &mut world.colliders);
                                        //what was I thinking here simulation.world.recurse_part_mut(part_handle, 0, 0, AttachedPartFacing::Up, AttachedPartFacing::Up, &mut |_handle, part: &mut world::parts::Part, _, _, _, _| part.join_to(player_meta));
                                        let mut parts_affected = Vec::new();
                                        parts_affected.push(part_handle);
                                        sim.recursive_detach_all(part_handle, &mut Some(player_meta), &mut parts_affected);
                                        player_meta.grabbed_part = Some((grabbed_id, sim.equip_mouse_dragging(part_handle, core_velocity), x, y));
                                        player_meta.parts_touching_planet.remove(&part_handle);
                                        if player_meta.parts_touching_planet.is_empty() { 
                                            player_meta.touching_planet = None;
                                            player_meta.beamout_status = BeamoutKind::None;
                                        }

                                        for part_affected in parts_affected {
                                            let part = sim.get_part(part_affected);
                                            free_parts.insert(part.id(), FreePart::Decaying(part_affected, DEFAULT_PART_DECAY_TICKS));
                                            outbound_events.push(ToSerializer::Broadcast(part.update_meta_msg()));
                                        }
                                        free_parts.insert(grabbed_id, FreePart::Grabbed(part_handle));
                                        outbound_events.push(ToSerializer::Broadcast(player_meta.update_meta_msg()));
                                        outbound_events.push(ToSerializer::Message(id, player_meta.update_my_meta()));
                                    };
                                }
                            }
                        }
                    },
                    ToServerMsg::MoveGrab{ x, y } => {
                        if let Some(player_meta) = players.get_mut(&id) {
                            if let Some((part_id, constraint, last_x, last_y)) = &mut player_meta.grabbed_part {
                                let core_velocity = sim.get_part_rigid(player_meta.core).linvel().clone();
                                //sim.update_mouse_dragging(constraint, x, y, core_velocity, &[player.);
                                let grabbed_part_handle = **free_parts.get(&part_id).expect("Mousen't3");
                                let body = sim.get_part_rigid_mut(grabbed_part_handle);
                                body.wake_up(false);
                                *last_x = x;
                                *last_y = y;
                            }
                        }
                    },
                    ToServerMsg::ReleaseGrab => {
                        if let Some(player_meta) = players.get_mut(&id) {
                            if player_meta.grabbed_part.is_some() {
                                let (part_id, mut constraint, x, y) = std::mem::replace(&mut player_meta.grabbed_part, None).unwrap();

                                let core_body = sim.get_part_rigid(player_meta.core);
                                let core_location = core_body.position().clone();
                                let core_velocity = core_body.linvel().clone();
                                let grabbed_part_handle = **free_parts.get(&part_id).unwrap();
                                let grabbed_part = sim.get_part_mut(grabbed_part_handle);
        
                                let x = core_location.translation.x + x;
                                let y = core_location.translation.y + y;
                                let clamp = sim.update_mouse_dragging(&mut constraint, x, y, core_velocity, &[player_meta.core]);
                                sim.release_mouse_constraint(constraint);

                                sim.recurse_part_mut(grabbed_part_handle, Default::default(), &mut |mut handle| {
                                    let grabbed_part_body = handle.self_rigid_mut();
                                    grabbed_part_body.set_linvel(core_velocity, true);
                                });

                                if let Some(clamp) = clamp {
                                    free_parts.remove(&part_id);
                                    sim.attach_part_player_agnostic(clamp.parent, grabbed_part_handle, clamp.attachment_slot);
                                    let (child, world) = sim.split_part_and_world(grabbed_part_handle);
                                    child.join_to(player_meta, &mut world.colliders);
                                    outbound_events.push(ToSerializer::Message(id, player_meta.update_my_meta()));
                                    use world::parts::CompactThrustMode;
                                    sim.recurse_part_mut(grabbed_part_handle, clamp.parent_visit_details.branch(Some(clamp.attachment_details)), &mut |mut part| {
                                        part.thrust_mode = CompactThrustMode::calculate_from_details(part.details(), part.thrust_mode.is_solid());
                                        outbound_events.push(ToSerializer::Broadcast(part.update_meta_msg()));
                                    });
                                } else {
                                    free_parts.get_mut(&part_id).unwrap().become_decaying();
                                }
        
                                outbound_events.push(ToSerializer::Broadcast(codec::ToClientMsg::UpdatePlayerMeta {
                                    id,
                                    thrust_forward: player_meta.thrust_forwards, thrust_backward: player_meta.thrust_backwards, thrust_clockwise: player_meta.thrust_clockwise, thrust_counter_clockwise: player_meta.thrust_counterclockwise,
                                    grabed_part: None
                                }));
                            }
                        }
                    },
                    ToServerMsg::BeamOut => {
                        if let Some(player) = players.get(&id) {
                            if matches!(player.beamout_status, BeamoutKind::Beamout | BeamoutKind::Dock) {
                                let player = players.remove(&id).unwrap();
                                let core = sim.get_part(player.core);
                                let beamout_layout = core.deflate(&sim);
                                outbound_events.push(ToSerializer::Broadcast(codec::ToClientMsg::BeamOutAnimation { player_id: id }));
                                let msg = match player.beamout_status {
                                    BeamoutKind::Beamout => "beamed out",
                                    BeamoutKind::Dock => "docked",
                                    BeamoutKind::None => "violated physics",
                                };
                                outbound_events.push(ToSerializer::Broadcast(codec::ToClientMsg::ChatMessage { username: "Server".to_owned(), msg: format!("{} {}", player.name, msg), color: "#87de87".to_owned() }));
                                let mut garbage_deletion_messages = Vec::new();
                                sim.delete_parts_recursive(player.core, &mut garbage_deletion_messages);
                                if let (Some(beamout_token), Some(api)) = (player.beamout_token, api.as_ref()) { 
                                    beamout::spawn_beamout_request(beamout_token, player.beamout_status, player.touching_planet, beamout_layout, api.clone());
                                };
                                let my_to_serializer = to_serializer.clone();
                                async_std::task::spawn(async move {
                                    futures_timer::Delay::new(std::time::Duration::from_millis(2500)).await;
                                    my_to_serializer.send(vec![ ToSerializer::DeleteWriter(id) ]).await;
                                });
                            }
                        }
                    },
                    _ => { outbound_events.push(ToSerializer::DeleteWriter(id)); }
                }
            },

            Event::InboundEvent(AdminCommand { id, command }) => {
                let chunks: Vec<String> = command.split_whitespace().map(|s| s.to_string()).collect();
                match chunks[0].as_str() {
                    "/teleport" => {
                        if chunks.len() == 3 {
                            if let (Ok(x), Ok(y)) = (chunks[1].parse::<f32>(), chunks[2].parse::<f32>()) {
                                let teleport_to = Vector::new(x, y);
                                if let Some(player_meta) = players.get_mut(&id) {
                                    let core_pos = sim.get_part_rigid(player_meta.core).position().translation.vector;
                                    println!("Teleporting {} to: {} {}", player_meta.name, x, y);
                                    sim.recurse_part_mut(player_meta.core, Default::default(), &mut |mut handle: world::PartVisitHandleMut| {
                                        let body = handle.self_rigid_mut();
                                        let pos = Isometry::new(
                                                body.position().clone().translation.vector - core_pos + teleport_to,
                                                body.position().rotation.angle()
                                        );
                                        body.set_position(pos, true);
                                    });
                                }
                            }
                        }
                    },

                    "/novel" => {
                        if let Some(player_meta) = players.get(&id) {
                            sim.recurse_part_mut(player_meta.core, Default::default(), &mut |mut handle: world::PartVisitHandleMut| {
                                let body = handle.self_rigid_mut();
                                body.set_linvel(Vector::new(0.0, 0.0), true);
                                body.set_angvel(0.0, true);
                            });
                        }
                    },
                    "/vel" => {
                        if let Some(player_meta) = players.get(&id) {
                            let body = sim.get_part_rigid(player_meta.core);
                            outbound_events.push(ToSerializer::Message(id, ToClientMsg::ChatMessage { username: "Server".to_owned(), color: "skyblue".to_owned(), msg: format!("X: {}; Y: {}; Rot: {}", body.linvel().x, body.linvel().y, body.angvel()) }));
                        }
                    },
                    "/reorientate" => {
                        if let Some(player_meta) = players.get(&id) {
                            let body = sim.get_part_rigid_mut(player_meta.core);
                            body.set_position(Isometry::new(body.position().translation.vector, 0.0), true);
                        }
                    },

                    "/refuel" => {
                        if let Some(player_meta) = players.get_mut(&id) {
                            (player_meta.power) = player_meta.max_power;
                        }
                    }

                    "/stop" => {
                        println!("{:?} called an emergency stop", players.get(&id).map(|player| &player.name));
                        emergency_stop(&players, &sim, &api).await;
                    },

                    _ => {
                        to_serializer.send(vec! [ToSerializerEvent::Message(id, ToClientMsg::ChatMessage{ username: String::from("Server"), msg: String::from("You cannot use that command"), color: String::from("#FF0000") })]).await;
                    }
                    
                }
            },

            Event::EmergencyStop => {
                println!("Recieved a signal or something");
                emergency_stop(&players, &sim, &api).await;
            }
        }
        to_serializer.send(outbound_events).await;
    }
}


fn broken_part_detach_finish(affected_parts: Vec<PartHandle>, sim: &mut world::Simulation, free_parts: &mut BTreeMap<u16, FreePart>, players: &mut BTreeMap<u16, PlayerMeta>, out: &mut Vec<ToSerializerEvent> ) {
    let mut players_affected = BTreeSet::new();
    for part_handle in affected_parts {
        let (part, world) = sim.split_part_and_world(part_handle);
        free_parts.insert(part.id(), FreePart::Decaying(part_handle, DEFAULT_PART_DECAY_TICKS));
        if let Some(player_id) = part.part_of_player() {
            if let Some(player) = players.get_mut(&player_id) {
                player.parts_touching_planet.remove(&part_handle);
                if player.parts_touching_planet.is_empty() {
                    player.beamout_status = BeamoutKind::None;
                    player.touching_planet = None;
                }
                part.make_playernt(Some(player), &mut world.colliders);
            } else {
                part.make_playernt(None, &mut world.colliders);
                println!("playernt? {:?}", part_handle);
            }
            players_affected.insert(player_id);
            out.push(ToSerializerEvent::Broadcast(part.update_meta_msg()));
        } else {
            panic!("UH OH Finsih Broken Partn't Detach");
        }
        out.push(ToSerializerEvent::Broadcast(part.update_meta_msg()));
    }
    for player_id in players_affected {
        if let Some(player) = players.get_mut(&player_id) {
            out.push(ToSerializerEvent::Message(player.id, player.update_my_meta()));
        }
    }
}

enum FreePart {
    Decaying(PartHandle, u16),
    EarthCargo(PartHandle, u16),
    Grabbed(PartHandle),
    PlaceholderLol,
}

impl FreePart {
    pub fn become_grabbed(&mut self, earth_cargo_count: &mut u8) {
        if let FreePart::EarthCargo(_, _) = self { *earth_cargo_count -= 1 };
        match self {
            FreePart::Decaying(part, _) | FreePart::EarthCargo(part, _) => { *self = FreePart::Grabbed(*part) }
            FreePart::PlaceholderLol | FreePart::Grabbed(_) => panic!("FreePart::Grabbed called on bad")
        }
    }
    pub fn become_decaying(&mut self) {
        match self {
            FreePart::Decaying(part, _) | FreePart::Grabbed(part) => { *self = FreePart::Decaying(*part, DEFAULT_PART_DECAY_TICKS) }
            FreePart::PlaceholderLol | FreePart::EarthCargo(_, _) => panic!("FreePart::Grabbed called on bad")
        }
    }
}
impl std::ops::Deref for FreePart {
    type Target = PartHandle;
    fn deref(&self) -> &PartHandle {
        match self {
            FreePart::Decaying(handle, _) | FreePart::EarthCargo(handle, _) | FreePart::Grabbed(handle) => handle,
            FreePart::PlaceholderLol => panic!("how did we get here")
        }
    }
}

pub fn rotate_vector_with_angle(x: f32, y: f32, theta: f32) -> (f32, f32) { rotate_vector(x, y, theta.sin(), theta.cos()) }
pub fn rotate_vector(x: f32, y: f32, theta_sin: f32, theta_cos: f32) -> (f32, f32) {
    ((x * theta_cos) - (y * theta_sin), (x * theta_sin) + (y * theta_cos))
}

pub struct PlayerMeta {
    pub id: u16,
    pub name: String,
    pub beamout_token: Option<String>, 

    pub core: PartHandle,
    pub thrust_forwards: bool,
    pub thrust_backwards: bool,
    pub thrust_clockwise: bool,
    pub thrust_counterclockwise: bool,

    pub power: u32,
    pub max_power: u32,
    pub power_regen_per_5_ticks: u32,

    pub grabbed_part: Option<(u16, world::MouseDraggingStuff, f32, f32)>,

    pub touching_planet: Option<u8>,
    ticks_til_cargo_transform: u8,
    parts_touching_planet: BTreeSet<PartHandle>,
    beamout_status: BeamoutKind,
}
impl PlayerMeta {
    fn new(my_id: u16, core_handle: PartHandle, name: String, beamout_token: Option<String>) -> PlayerMeta { PlayerMeta {
        id: my_id,
        core: core_handle,
        name,
        beamout_token,
        thrust_backwards: false, thrust_clockwise: false, thrust_counterclockwise: false, thrust_forwards: false,
        //power: 100 * crate::TICKS_PER_SECOND as u32, max_power: 100 * crate::TICKS_PER_SECOND as u32,
        power: 0, max_power: 0,
        power_regen_per_5_ticks: 0,
        grabbed_part: None,
        touching_planet: None,
        parts_touching_planet: BTreeSet::new(),
        ticks_til_cargo_transform: TICKS_PER_SECOND,
        beamout_status: BeamoutKind::None,
    } }

    fn update_meta_msg(&self) -> ToClientMsg {
        ToClientMsg::UpdatePlayerMeta {
            id: self.id,
            grabed_part: self.grabbed_part.as_ref().map(|(id, _, _, _)| *id),
            thrust_forward: self.thrust_forwards,
            thrust_backward: self.thrust_backwards,
            thrust_clockwise: self.thrust_clockwise,
            thrust_counter_clockwise: self.thrust_counterclockwise,
        }
    }
    fn update_my_meta(&self) -> ToClientMsg {
        ToClientMsg::UpdateMyMeta {
            max_power: self.max_power,
            beamout: self.beamout_status,
        }
    }
}
pub struct PartOfPlayer (u16);

async fn emergency_stop(players: &BTreeMap<u16, PlayerMeta>, world: &world::Simulation, api: &Option<Arc<ApiDat>>) {
    unsafe { EMERGENCY_STOP.store(true, AtomicOrdering::Release) };
    println!("EMERGENCY STOP");
    if let Some(api) = api {
        for player in players.values() {
            let core = world.get_part(player.core);
            let beamout_layout = core.deflate(world);
            if let Some(beamout_token) = &player.beamout_token {
                println!("Beaming out {} (token: {})", player.name, beamout_token);
                beamout::spawn_beamout_request(beamout_token.to_owned(), BeamoutKind::Beamout, None, beamout_layout, api.clone()).await; 
                println!("Finished taht player");
            } else {
                println!("Player {} has no beamout token", player.name);
            }
        }
    }
    std::process::exit(1);
}
