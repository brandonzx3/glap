export class Box<T> {
    v: T;
    constructor(v: T) { this.v = v; }
}

function type_string_serialize(out: number[], string: string) {
    if (string.length > 255) { out.push(0); }
    else {
        out.push(string.length);
        for (let i = 0; i < string.length; i++) out.push(string.charCodeAt(i));
    }
}
function type_string_deserialize(buf: Uint8Array, index: Box<number>): string {
    let out = "";
    const size = buf[index.v];
    index.v++;
    let i = index.v;	
    index.v += size;
    while (i < index.v) out += String.fromCharCode(buf[i++]);
    return out; 
}

function type_float_serialize(out: number[], float: number) {
    const arr = new Float32Array([float]);
    const view = new Uint8Array(arr.buffer);
    out.push(view[3], view[2], view[1], view[0]);
}
function type_float_deserialize(buf: Uint8Array, index: Box<number>): number {
    const arr = new Uint8Array([buf[index.v+3], buf[index.v+2], buf[index.v+1], buf[index.v]]);
    const view = new Float32Array(arr.buffer);
    index.v += 4;
    return view[0];
}

function type_ushort_serialize(out: number[], ushort: number) {
    const arr = new Uint16Array([ushort]);
    const view = new Uint8Array(arr.buffer);
    out.push(view[1], view[0]);
}
function type_ushort_deserialize(buf: Uint8Array, index: Box<number>): number {
    const arr = new Uint8Array([buf[index.v+1], buf[index.v]]);
    const view = new Uint16Array(arr.buffer);
    index.v += 2;
    return view[0];
}

function type_uint_serialize(out: number[], uint: number) {
    const arr = new Uint32Array([uint]);
    const view = new Uint8Array(arr.buffer);
    out.push(view[3], view[2], view[1], view[0]);
}
function type_uint_deserialize(buf: Uint8Array, index: Box<number>): number {
    const arr = new Uint8Array([buf[index.v+3], buf[index.v+2], buf[index.v+1], buf[index.v]]);
    const view = new Uint32Array(arr.buffer);
	index.v += 4;
    return view[0];
}

function type_float_pair_serialize(out: number[], pair: [number, number]) {
    type_float_serialize(out, pair[0])
    type_float_serialize(out, pair[1]);
}
function type_float_pair_deserialize(buf: Uint8Array, index: Box<number>): [number, number] {
    return [type_float_deserialize(buf, index), type_float_deserialize(buf, index)];
}

function type_ubyte_serialize(out: number[], ubyte: number) { out.push(ubyte); }
function type_ubyte_deserialize(buf: Uint8Array, index: Box<number>): number { return buf[index.v++]; }

function type_boolean_serialize(out: number[], bool: boolean) { out.push(bool ? 1 : 0); }
function type_boolean_deserialize(buf: Uint8Array, index: Box<number>): boolean { return buf[index.v++] > 0; }


#[derive(Copy, Clone, PartialEq, Eq, Debug)]
export enum PartKind {
	Cargo = 1,
	Core = 0,
	CoreLite = 13,
	EcoThruster = 5,
	Hub = 3,
	HubThruster = 9,
	LandingThruster = 2,
	LandingThrusterSuspension = 12,
	LandingWheel = 10,
	LandingWheelSuspension = 11,
	PowerHub = 8,
	SolarPanel = 4,
	SuperThruster = 7,
	Thruster = 6,
}
export function enum_PartKind_serialize(buf: number[], val: PartKind) {

buf.push(val as number);
}
export function enum_PartKind_deserialize(buf: Uint8Array, index: Box<number>): PartKind {
	const value = buf[index.v++];
	if (value in PartKind) return value as PartKind;
	throw new Error("Failed to deserialize PartKind with value " + value);
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
export enum PlanetKind {
	Earth = 0,
	Jupiter = 8,
	Mars = 2,
	Mercury = 5,
	Moon = 3,
	Neptune = 6,
	Pluto = 10,
	Saturn = 9,
	Sun = 4,
	Trade = 11,
	Uranus = 7,
	Venus = 1,
}
export function enum_PlanetKind_serialize(buf: number[], val: PlanetKind) {

buf.push(val as number);
}
export function enum_PlanetKind_deserialize(buf: Uint8Array, index: Box<number>): PlanetKind {
	const value = buf[index.v++];
	if (value in PlanetKind) return value as PlanetKind;
	throw new Error("Failed to deserialize PlanetKind with value " + value);
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
export enum BeamoutKind {
	Beamout = 0,
	Dock = 1,
	None = 2,
}
export function enum_BeamoutKind_serialize(buf: number[], val: BeamoutKind) {

buf.push(val as number);
}
export function enum_BeamoutKind_deserialize(buf: Uint8Array, index: Box<number>): BeamoutKind {
	const value = buf[index.v++];
	if (value in BeamoutKind) return value as BeamoutKind;
	throw new Error("Failed to deserialize BeamoutKind with value " + value);
}

export interface ToServerMsg {}
class ToServerMsg_BeamOut implements ToServerMsg {
	static readonly id = 0;
	constructor(
	) {}
	serialize(): Uint8Array {
		let out = [0];
		let item: unknown = null;
		return new Uint8Array(out);
	}
}
class ToServerMsg_CommitGrab implements ToServerMsg {
	static readonly id = 1;
	constructor(
		public grabbed_id: number,
		public x: number,
		public y: number,
	) {}
	serialize(): Uint8Array {
		let out = [1];
		let item: unknown = null;
		item = this.grabbed_id; type_ushort_serialize(out, item);
		item = this.x; type_float_serialize(out, item);
		item = this.y; type_float_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToServerMsg_Handshake implements ToServerMsg {
	static readonly id = 2;
	constructor(
		public client: string,
		public name: string,
		public session: string,
	) {}
	serialize(): Uint8Array {
		let out = [2];
		let item: unknown = null;
		item = this.client; type_string_serialize(out, item);
		item = this.name; type_string_serialize(out, item);
		item = this.session; type_string_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToServerMsg_MoveGrab implements ToServerMsg {
	static readonly id = 3;
	constructor(
		public x: number,
		public y: number,
	) {}
	serialize(): Uint8Array {
		let out = [3];
		let item: unknown = null;
		item = this.x; type_float_serialize(out, item);
		item = this.y; type_float_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToServerMsg_ReleaseGrab implements ToServerMsg {
	static readonly id = 4;
	constructor(
	) {}
	serialize(): Uint8Array {
		let out = [4];
		let item: unknown = null;
		return new Uint8Array(out);
	}
}
class ToServerMsg_RequestUpdate implements ToServerMsg {
	static readonly id = 5;
	constructor(
	) {}
	serialize(): Uint8Array {
		let out = [5];
		let item: unknown = null;
		return new Uint8Array(out);
	}
}
class ToServerMsg_SendChatMessage implements ToServerMsg {
	static readonly id = 6;
	constructor(
		public msg: string,
	) {}
	serialize(): Uint8Array {
		let out = [6];
		let item: unknown = null;
		item = this.msg; type_string_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToServerMsg_SetThrusters implements ToServerMsg {
	static readonly id = 7;
	constructor(
		public backward: boolean,
		public clockwise: boolean,
		public counter_clockwise: boolean,
		public forward: boolean,
	) {}
	serialize(): Uint8Array {
		let out = [7];
		let item: unknown = null;
		item = this.backward; type_boolean_serialize(out, item);
		item = this.clockwise; type_boolean_serialize(out, item);
		item = this.counter_clockwise; type_boolean_serialize(out, item);
		item = this.forward; type_boolean_serialize(out, item);
		return new Uint8Array(out);
	}
}
function deserialize_ToServerMsg(buf: Uint8Array, index: Box<number>) {
	switch (buf[index.v++]) {
		let item: unknown = null;
		case 0: {
			return new ToServerMsg_BeamOut();
		}; break;
		let item: unknown = null;
		case 1: {
			item = type_ushort_deserialize(buf, index);; let grabbed_id = item;
			item = type_float_deserialize(buf, index);; let x = item;
			item = type_float_deserialize(buf, index);; let y = item;
			return new ToServerMsg_CommitGrab(grabbed_id, x, y);
		}; break;
		let item: unknown = null;
		case 2: {
			item = type_string_deserialize(buf, index);; let client = item;
			item = type_string_deserialize(buf, index);; let name = item;
			item = type_string_deserialize(buf, index);; let session = item;
			return new ToServerMsg_Handshake(client, name, session);
		}; break;
		let item: unknown = null;
		case 3: {
			item = type_float_deserialize(buf, index);; let x = item;
			item = type_float_deserialize(buf, index);; let y = item;
			return new ToServerMsg_MoveGrab(x, y);
		}; break;
		let item: unknown = null;
		case 4: {
			return new ToServerMsg_ReleaseGrab();
		}; break;
		let item: unknown = null;
		case 5: {
			return new ToServerMsg_RequestUpdate();
		}; break;
		let item: unknown = null;
		case 6: {
			item = type_string_deserialize(buf, index);; let msg = item;
			return new ToServerMsg_SendChatMessage(msg);
		}; break;
		let item: unknown = null;
		case 7: {
			item = type_boolean_deserialize(buf, index);; let backward = item;
			item = type_boolean_deserialize(buf, index);; let clockwise = item;
			item = type_boolean_deserialize(buf, index);; let counter_clockwise = item;
			item = type_boolean_deserialize(buf, index);; let forward = item;
			return new ToServerMsg_SetThrusters(backward, clockwise, counter_clockwise, forward);
		}; break;
		default: throw new Error("Invalid message type for ToServerMsg");
	};
}

export interface ToClientMsg {}
class ToClientMsg_AddCelestialObject implements ToClientMsg {
	static readonly id = 0;
	constructor(
		public id: number,
		public kind: PlanetKind,
		public position: [number, number],
		public radius: number,
	) {}
	serialize(): Uint8Array {
		let out = [0];
		let item: unknown = null;
		item = this.id; type_ubyte_serialize(out, item);
		item = this.kind; item.serialize(out);
		item = this.position; type_float_pair_serialize(out, item);
		item = this.radius; type_float_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_AddPart implements ToClientMsg {
	static readonly id = 1;
	constructor(
		public id: number,
		public kind: PartKind,
	) {}
	serialize(): Uint8Array {
		let out = [1];
		let item: unknown = null;
		item = this.id; type_ushort_serialize(out, item);
		item = this.kind; item.serialize(out);
		return new Uint8Array(out);
	}
}
class ToClientMsg_AddPlayer implements ToClientMsg {
	static readonly id = 2;
	constructor(
		public core_id: number,
		public id: number,
		public name: string,
	) {}
	serialize(): Uint8Array {
		let out = [2];
		let item: unknown = null;
		item = this.core_id; type_ushort_serialize(out, item);
		item = this.id; type_ushort_serialize(out, item);
		item = this.name; type_string_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_BeamOutAnimation implements ToClientMsg {
	static readonly id = 3;
	constructor(
		public player_id: number,
	) {}
	serialize(): Uint8Array {
		let out = [3];
		let item: unknown = null;
		item = this.player_id; type_ushort_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_ChatMessage implements ToClientMsg {
	static readonly id = 4;
	constructor(
		public color: string,
		public msg: string,
		public username: string,
	) {}
	serialize(): Uint8Array {
		let out = [4];
		let item: unknown = null;
		item = this.color; type_string_serialize(out, item);
		item = this.msg; type_string_serialize(out, item);
		item = this.username; type_string_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_Flow implements ToClientMsg {
	static readonly id = 5;
	constructor(
		public tick: number,
	) {}
	serialize(): Uint8Array {
		let out = [5];
		let item: unknown = null;
		item = this.tick; type_uint_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_HandshakeAccepted implements ToClientMsg {
	static readonly id = 6;
	constructor(
		public can_beamout: boolean,
		public core_id: number,
		public id: number,
	) {}
	serialize(): Uint8Array {
		let out = [6];
		let item: unknown = null;
		item = this.can_beamout; type_boolean_serialize(out, item);
		item = this.core_id; type_ushort_serialize(out, item);
		item = this.id; type_ushort_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_IncinerationAnimation implements ToClientMsg {
	static readonly id = 7;
	constructor(
		public player_id: number,
	) {}
	serialize(): Uint8Array {
		let out = [7];
		let item: unknown = null;
		item = this.player_id; type_ushort_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_InitCelestialOrbit implements ToClientMsg {
	static readonly id = 8;
	constructor(
		public id: number,
		public orbit_around_body: number,
		public orbit_radius: [number, number],
		public orbit_rotation: number,
		public orbit_tick_offset: number,
		public orbit_total_ticks: number,
	) {}
	serialize(): Uint8Array {
		let out = [8];
		let item: unknown = null;
		item = this.id; type_ubyte_serialize(out, item);
		item = this.orbit_around_body; type_ubyte_serialize(out, item);
		item = this.orbit_radius; type_float_pair_serialize(out, item);
		item = this.orbit_rotation; type_float_serialize(out, item);
		item = this.orbit_tick_offset; type_uint_serialize(out, item);
		item = this.orbit_total_ticks; type_uint_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_MessagePack implements ToClientMsg {
	static readonly id = 9;
	constructor(
		public count: number,
	) {}
	serialize(): Uint8Array {
		let out = [9];
		let item: unknown = null;
		item = this.count; type_ushort_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_MovePart implements ToClientMsg {
	static readonly id = 10;
	constructor(
		public id: number,
		public rotation_i: number,
		public rotation_n: number,
		public x: number,
		public y: number,
	) {}
	serialize(): Uint8Array {
		let out = [10];
		let item: unknown = null;
		item = this.id; type_ushort_serialize(out, item);
		item = this.rotation_i; type_float_serialize(out, item);
		item = this.rotation_n; type_float_serialize(out, item);
		item = this.x; type_float_serialize(out, item);
		item = this.y; type_float_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_PostSimulationTick implements ToClientMsg {
	static readonly id = 11;
	constructor(
		public your_power: number,
	) {}
	serialize(): Uint8Array {
		let out = [11];
		let item: unknown = null;
		item = this.your_power; type_uint_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_RemovePart implements ToClientMsg {
	static readonly id = 12;
	constructor(
		public id: number,
	) {}
	serialize(): Uint8Array {
		let out = [12];
		let item: unknown = null;
		item = this.id; type_ushort_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_RemovePlayer implements ToClientMsg {
	static readonly id = 13;
	constructor(
		public id: number,
	) {}
	serialize(): Uint8Array {
		let out = [13];
		let item: unknown = null;
		item = this.id; type_ushort_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_UpdateMyMeta implements ToClientMsg {
	static readonly id = 14;
	constructor(
		public beamout: BeamoutKind,
		public max_power: number,
	) {}
	serialize(): Uint8Array {
		let out = [14];
		let item: unknown = null;
		item = this.beamout; item.serialize(out);
		item = this.max_power; type_uint_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_UpdatePartMeta implements ToClientMsg {
	static readonly id = 15;
	constructor(
		public id: number,
		public owning_player: number | null,
		public thrust_mode: number,
	) {}
	serialize(): Uint8Array {
		let out = [15];
		let item: unknown = null;
		item = this.id; type_ushort_serialize(out, item);
		item = this.owning_player; if (item == null) out.push(0); else { out.push(1); type_ushort_serialize(out, item); }
		item = this.thrust_mode; type_ubyte_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_UpdatePlayerMeta implements ToClientMsg {
	static readonly id = 16;
	constructor(
		public grabbed_part: number | null,
		public id: number,
		public thrust_backward: boolean,
		public thrust_clockwise: boolean,
		public thrust_counter_clockwise: boolean,
		public thrust_forward: boolean,
	) {}
	serialize(): Uint8Array {
		let out = [16];
		let item: unknown = null;
		item = this.grabbed_part; if (item == null) out.push(0); else { out.push(1); type_ushort_serialize(out, item); }
		item = this.id; type_ushort_serialize(out, item);
		item = this.thrust_backward; type_boolean_serialize(out, item);
		item = this.thrust_clockwise; type_boolean_serialize(out, item);
		item = this.thrust_counter_clockwise; type_boolean_serialize(out, item);
		item = this.thrust_forward; type_boolean_serialize(out, item);
		return new Uint8Array(out);
	}
}
class ToClientMsg_UpdatePlayerVelocity implements ToClientMsg {
	static readonly id = 17;
	constructor(
		public id: number,
		public vel_x: number,
		public vel_y: number,
	) {}
	serialize(): Uint8Array {
		let out = [17];
		let item: unknown = null;
		item = this.id; type_ushort_serialize(out, item);
		item = this.vel_x; type_float_serialize(out, item);
		item = this.vel_y; type_float_serialize(out, item);
		return new Uint8Array(out);
	}
}
function deserialize_ToClientMsg(buf: Uint8Array, index: Box<number>) {
	switch (buf[index.v++]) {
		let item: unknown = null;
		case 0: {
			item = type_ubyte_deserialize(buf, index);; let id = item;
			item = PlanetKind.deserialize(buf, index);; let kind = item;
			item = type_float_pair_deserialize(buf, index);; let position = item;
			item = type_float_deserialize(buf, index);; let radius = item;
			return new ToClientMsg_AddCelestialObject(id, kind, position, radius);
		}; break;
		let item: unknown = null;
		case 1: {
			item = type_ushort_deserialize(buf, index);; let id = item;
			item = PartKind.deserialize(buf, index);; let kind = item;
			return new ToClientMsg_AddPart(id, kind);
		}; break;
		let item: unknown = null;
		case 2: {
			item = type_ushort_deserialize(buf, index);; let core_id = item;
			item = type_ushort_deserialize(buf, index);; let id = item;
			item = type_string_deserialize(buf, index);; let name = item;
			return new ToClientMsg_AddPlayer(core_id, id, name);
		}; break;
		let item: unknown = null;
		case 3: {
			item = type_ushort_deserialize(buf, index);; let player_id = item;
			return new ToClientMsg_BeamOutAnimation(player_id);
		}; break;
		let item: unknown = null;
		case 4: {
			item = type_string_deserialize(buf, index);; let color = item;
			item = type_string_deserialize(buf, index);; let msg = item;
			item = type_string_deserialize(buf, index);; let username = item;
			return new ToClientMsg_ChatMessage(color, msg, username);
		}; break;
		let item: unknown = null;
		case 5: {
			item = type_uint_deserialize(buf, index);; let tick = item;
			return new ToClientMsg_Flow(tick);
		}; break;
		let item: unknown = null;
		case 6: {
			item = type_boolean_deserialize(buf, index);; let can_beamout = item;
			item = type_ushort_deserialize(buf, index);; let core_id = item;
			item = type_ushort_deserialize(buf, index);; let id = item;
			return new ToClientMsg_HandshakeAccepted(can_beamout, core_id, id);
		}; break;
		let item: unknown = null;
		case 7: {
			item = type_ushort_deserialize(buf, index);; let player_id = item;
			return new ToClientMsg_IncinerationAnimation(player_id);
		}; break;
		let item: unknown = null;
		case 8: {
			item = type_ubyte_deserialize(buf, index);; let id = item;
			item = type_ubyte_deserialize(buf, index);; let orbit_around_body = item;
			item = type_float_pair_deserialize(buf, index);; let orbit_radius = item;
			item = type_float_deserialize(buf, index);; let orbit_rotation = item;
			item = type_uint_deserialize(buf, index);; let orbit_tick_offset = item;
			item = type_uint_deserialize(buf, index);; let orbit_total_ticks = item;
			return new ToClientMsg_InitCelestialOrbit(id, orbit_around_body, orbit_radius, orbit_rotation, orbit_tick_offset, orbit_total_ticks);
		}; break;
		let item: unknown = null;
		case 9: {
			item = type_ushort_deserialize(buf, index);; let count = item;
			return new ToClientMsg_MessagePack(count);
		}; break;
		let item: unknown = null;
		case 10: {
			item = type_ushort_deserialize(buf, index);; let id = item;
			item = type_float_deserialize(buf, index);; let rotation_i = item;
			item = type_float_deserialize(buf, index);; let rotation_n = item;
			item = type_float_deserialize(buf, index);; let x = item;
			item = type_float_deserialize(buf, index);; let y = item;
			return new ToClientMsg_MovePart(id, rotation_i, rotation_n, x, y);
		}; break;
		let item: unknown = null;
		case 11: {
			item = type_uint_deserialize(buf, index);; let your_power = item;
			return new ToClientMsg_PostSimulationTick(your_power);
		}; break;
		let item: unknown = null;
		case 12: {
			item = type_ushort_deserialize(buf, index);; let id = item;
			return new ToClientMsg_RemovePart(id);
		}; break;
		let item: unknown = null;
		case 13: {
			item = type_ushort_deserialize(buf, index);; let id = item;
			return new ToClientMsg_RemovePlayer(id);
		}; break;
		let item: unknown = null;
		case 14: {
			item = BeamoutKind.deserialize(buf, index);; let beamout = item;
			item = type_uint_deserialize(buf, index);; let max_power = item;
			return new ToClientMsg_UpdateMyMeta(beamout, max_power);
		}; break;
		let item: unknown = null;
		case 15: {
			item = type_ushort_deserialize(buf, index);; let id = item;
			if (buf[index.v++] > 0 { item = type_ushort_deserialize(buf, index); } else item = null;; let owning_player = item;
			item = type_ubyte_deserialize(buf, index);; let thrust_mode = item;
			return new ToClientMsg_UpdatePartMeta(id, owning_player, thrust_mode);
		}; break;
		let item: unknown = null;
		case 16: {
			if (buf[index.v++] > 0 { item = type_ushort_deserialize(buf, index); } else item = null;; let grabbed_part = item;
			item = type_ushort_deserialize(buf, index);; let id = item;
			item = type_boolean_deserialize(buf, index);; let thrust_backward = item;
			item = type_boolean_deserialize(buf, index);; let thrust_clockwise = item;
			item = type_boolean_deserialize(buf, index);; let thrust_counter_clockwise = item;
			item = type_boolean_deserialize(buf, index);; let thrust_forward = item;
			return new ToClientMsg_UpdatePlayerMeta(grabbed_part, id, thrust_backward, thrust_clockwise, thrust_counter_clockwise, thrust_forward);
		}; break;
		let item: unknown = null;
		case 17: {
			item = type_ushort_deserialize(buf, index);; let id = item;
			item = type_float_deserialize(buf, index);; let vel_x = item;
			item = type_float_deserialize(buf, index);; let vel_y = item;
			return new ToClientMsg_UpdatePlayerVelocity(id, vel_x, vel_y);
		}; break;
		default: throw new Error("Invalid message type for ToClientMsg");
	};
}

